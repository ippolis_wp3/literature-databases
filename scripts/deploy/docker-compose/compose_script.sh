#!/bin/bash
SCRIPT_DIRECTORY=$(dirname "${BASH_SOURCE[0]}")
PROJECT_DIRECTORY=$( cd "$SCRIPT_DIRECTORY/../../../services" ; pwd -P )
ENV_FILE=$SCRIPT_DIRECTORY/.env && test -f $ENV_FILE && source $ENV_FILE
COMPOSE_BINARY=${DOCKER_COMPOSE_BINARY:-sudo docker compose}

$COMPOSE_BINARY -p ippolis_wp3 --project-directory "$PROJECT_DIRECTORY" -f "$SCRIPT_DIRECTORY/docker-compose-$1.yml" "${@:2}"
