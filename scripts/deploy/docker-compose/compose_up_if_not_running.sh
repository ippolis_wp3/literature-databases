#!/bin/bash
SCRIPT_DIRECTORY=$(dirname "${BASH_SOURCE[0]}")
BACKEND_ID=$(docker ps -q -f name=ippolis_wp3_backend_1)

if [ "$BACKEND_ID" = "" ]; then
  $SCRIPT_DIRECTORY/compose_restart.sh
fi
