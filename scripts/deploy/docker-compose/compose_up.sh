#!/bin/bash
SCRIPT_DIRECTORY=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
TARGET=${1-dev}

"$SCRIPT_DIRECTORY/compose_script.sh" $TARGET up --build
