package de.ippolis.wp3.local_database_literature_service.dblp.service;

import de.ippolis.wp3.local_database_literature_service.utils.helper.BibTexReferenceComparisonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class DBLPAPIService {

  private final RawRestServiceDBLP rawRestServiceDBLP;
  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;

  @Autowired
  public DBLPAPIService(
      RawRestServiceDBLP rawRestServiceDBLP,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper) {
    this.rawRestServiceDBLP = rawRestServiceDBLP;

    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
  }

  @Cacheable(value = "dblpArxivID", unless = "#result==null")
  public String fetchDBLPByArxivID(String id) {
    if (id.equals("")) {
      return null;
    }
    String basicUrl = "https://dblp.org/search/publ/api?q=";
    String url = "";
    String titlePre = bibTexReferenceComparisonHelper.preProcessName(id);
    url = basicUrl + titlePre + "&format=json&h=5";
    try {
      final String response = rawRestServiceDBLP.performJsonGetRequest(url);
      return (response);
    } catch (Exception e) {
      return (null);
    }
  }

  @Cacheable(value = "dblpVenue", unless = "#result==null")
  public String fetchDBLPVenueByVenueId(String id) {
    if (id.equals("")) {
      return null;
    }
    String basicUrl = "https://dblp.org/search/venue/api?q=";
    String url = "";
    url = basicUrl + id + "&format=json&h=5";
    try {
      final String response = rawRestServiceDBLP.performJsonGetRequest(url);
      return (response);
    } catch (Exception e) {
      return (null);
    }
  }

  @Cacheable(value = "dblpTitle", unless = "#result==null")
  public String fetchDBLPByTitle(String title) {
    if (title.equals("")) {
      return null;
    }
    String basicUrl = "https://dblp.org/search/publ/api?q=";
    String url = "";
    String titlePre = bibTexReferenceComparisonHelper.preProcessName(title);
    url = basicUrl + titlePre + "&format=json&h=5";
    try {
      final String response = rawRestServiceDBLP.performJsonGetRequest(url);
      return (response);
    } catch (Exception e) {
      return (null);
    }
  }
}
