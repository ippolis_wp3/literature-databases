package de.ippolis.wp3.local_database_literature_service.utils.helper;

public class TextRange {

  private int start;
  private int end;

  public TextRange(int start, int end) {
    this.start = start;
    this.end = end;
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getEnd() {
    return end;
  }

  public void setEnd(int end) {
    this.end = end;
  }
}
