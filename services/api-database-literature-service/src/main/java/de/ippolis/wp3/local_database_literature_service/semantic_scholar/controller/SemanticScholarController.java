package de.ippolis.wp3.local_database_literature_service.semantic_scholar.controller;

import de.ippolis.wp3.local_database_literature_service.semantic_scholar.service.SemanticScholarAPIReferenceService;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.ArxivIdBibTexPagePreprocessingRequestDTO;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.DoiBibTexPagePreprocessingRequestDTO;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.TitleBibTexPagePreprocessingRequestDTO;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.SuccessJsonResponse;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SemanticScholarController {

  private final SemanticScholarAPIReferenceService semanticScholarAPIReferenceService;
  Logger logger = LoggerFactory.getLogger(SemanticScholarController.class);

  public SemanticScholarController(
      SemanticScholarAPIReferenceService semanticScholarAPIReferenceService) {
    this.semanticScholarAPIReferenceService = semanticScholarAPIReferenceService;
  }

  @PostMapping(
      value = "/semantic-scholar/getAPIReferencesByTitle",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCiteByTitle(
      @RequestBody TitleBibTexPagePreprocessingRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      String id = requestDTO.getId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          semanticScholarAPIReferenceService.getReferencesByTitle(title, bibTexPagePreprocessing,
              id);
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/getAPIReferencesByDOI",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCiteByDOI(
      @RequestBody DoiBibTexPagePreprocessingRequestDTO requestDTO) {
    try {
      String doi = requestDTO.getDoi();
      String id = requestDTO.getId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          semanticScholarAPIReferenceService.getReferencesByDOI(doi, bibTexPagePreprocessing, id);
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/getAPIReferencesByArxivId",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCiteByArxivId(
      @RequestBody ArxivIdBibTexPagePreprocessingRequestDTO requestDTO) {
    try {
      String arxivId = requestDTO.getArxivId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          semanticScholarAPIReferenceService.getReferencesByArxiv(arxivId, bibTexPagePreprocessing);
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

}
