package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import java.util.List;

public class ArxivDoiTitleAuthorRequestDTO {

  private String id;
  private String doi;
  private String arxivId;
  private String title;
  private List<String> authors;
  private BibTexPagePreprocessing bibTexPagePreprocessing;
  private boolean useLocalData;
  private boolean useAPI;

  public ArxivDoiTitleAuthorRequestDTO() {
  }

  public ArxivDoiTitleAuthorRequestDTO(
      String id,
      String doi,
      String arxivId,
      String title,
      List<String> authors,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      boolean useLocalData,
      boolean useAPI) {
    this.id = id;
    this.doi = doi;
    this.arxivId = arxivId;
    this.title = title;
    this.authors = authors;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
    this.useLocalData = useLocalData;
    this.useAPI = useAPI;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getArxivId() {
    return arxivId;
  }

  public void setArxivId(String arxivId) {
    this.arxivId = arxivId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public boolean isUseLocalData() {
    return useLocalData;
  }

  public void setUseLocalData(boolean useLocalData) {
    this.useLocalData = useLocalData;
  }

  public boolean isUseAPI() {
    return useAPI;
  }

  public void setUseAPI(boolean useAPI) {
    this.useAPI = useAPI;
  }
}
