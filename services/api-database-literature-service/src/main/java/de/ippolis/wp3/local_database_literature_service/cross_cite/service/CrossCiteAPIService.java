package de.ippolis.wp3.local_database_literature_service.cross_cite.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class CrossCiteAPIService {

  private final RawRestServiceCrossCite rawRestServiceCrossCite;
  private final RawRestServiceCrossRef rawRestServiceCrossRef;
  Logger logger = LoggerFactory.getLogger(CrossCiteAPIService.class);

  @Autowired
  public CrossCiteAPIService(
      RawRestServiceCrossCite rawRestServiceCrossCite,
      RawRestServiceCrossRef rawRestServiceCrossRef) {
    this.rawRestServiceCrossCite = rawRestServiceCrossCite;
    this.rawRestServiceCrossRef = rawRestServiceCrossRef;
  }

  @Cacheable(value = "crossCiteDOI", unless = "#result==null")
  public String fetchCitationCrossCite(String doi) {
    if (doi.equals("")) {
      return null;
    }
    String doiUrl = "https://doi.org/" + doi;
    try {
      final String response = rawRestServiceCrossCite.performJsonGetRequest(doiUrl);
      return (response);
    } catch (Exception e) {
      logger.debug(e.getMessage());
      return (null);
    }
  }

  @Cacheable(value = "crossCiteTitle", unless = "#result==null")
  public String fetchCitationCrossRef(String title) {
    if (title.equals("")) {
      return null;
    }
    String doiUrl =
        "https://api.crossref.org/works?query.bibliographic=" + title + "&sort=score&rows=10";
    try {
      final String response = rawRestServiceCrossRef.performJsonGetRequest(doiUrl);
      return (response);
    } catch (Exception e) {
      logger.debug("Error during fetching of CrossRef information by title: " + e.getMessage());
      return (null);
    }
  }
}
