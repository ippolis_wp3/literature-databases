package de.ippolis.wp3.local_database_literature_service.semantic_scholar.service;

import de.ippolis.wp3.local_database_literature_service.utils.helper.ResourceHelperService;
import java.io.IOException;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Provides REST functionality
 */
@Service
public class RawRestServiceSemanticScholar {

  private final RestTemplate rawRestTemplate;
  private final Resource config;
  private final ResourceHelperService resourceHelperService;

  @Autowired
  public RawRestServiceSemanticScholar(
      RestTemplate rawRestTemplate,
      @Value("classpath:secret/semanticScholarConfig.txt") Resource config,
      ResourceHelperService resourceHelperService) {
    this.rawRestTemplate = rawRestTemplate;
    this.config = config;
    this.resourceHelperService = resourceHelperService;
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, trying to deserialize
   * the result into the given response type
   */
  public <T> T performJsonGetRequest(String requestUri, Class<T> responseType) throws IOException {
    HttpHeaders headers = new HttpHeaders();
    if (config.exists()) {
      List<String> lines = this.resourceHelperService.readLinesFromResource(config);
      if (lines.size() > 0) {
        String key = lines.get(0);
        headers.set("x-api-key", key);
      }
    }

    HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

    return rawRestTemplate
        .exchange(requestUri, HttpMethod.GET, requestEntity, responseType)
        .getBody();
  }

  /**
   * Performs a JSON POST request to the given URL with the given parameters, converting the result
   * into a {@link JSONObject}
   */
  public String performJsonGetRequest(String requestUri) throws IOException {
    String responseString = this.performJsonGetRequest(requestUri, String.class);

    return responseString;
  }
}
