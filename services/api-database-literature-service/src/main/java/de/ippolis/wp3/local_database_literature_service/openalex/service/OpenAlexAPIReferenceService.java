package de.ippolis.wp3.local_database_literature_service.openalex.service;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import de.ippolis.wp3.local_database_literature_service.utils.model.ReferenceSource;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class OpenAlexAPIReferenceService {

  private final OpenAlexAPIService openAlexAPIService;

  public OpenAlexAPIReferenceService(OpenAlexAPIService openAlexAPIService) {
    this.openAlexAPIService = openAlexAPIService;
  }

  public List<Reference> getOpenAlexResultsByTitle(
      String title, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {

    List<Reference> results = new LinkedList<>();
    String jsonObjectString = openAlexAPIService.fetchCitationByTitleOpenAlex(title);
    if (jsonObjectString == null) {
      return results;
    }
    Map<String, Object> jsonObject = new JSONObject(jsonObjectString).toMap();
    if (jsonObject == null) {
      return results;
    } else {
      if (jsonObject.containsKey("results")) {
        ArrayList<Map<String, Object>> resultsList =
            (ArrayList<Map<String, Object>>) jsonObject.get("results");
        for (Map<String, Object> resultsObject : resultsList) {
          Reference ref =
              new Reference(resultsObject, id, ReferenceSource.OPENALEX, bibTexPagePreprocessing);
          results.add(ref);
        }
      }
    }
    return results;
  }

  public List<Reference> getOpenAlexResultByDOI(
      String doi, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    List<Reference> references = new LinkedList<>();
    String jsonObjectFoundString = openAlexAPIService.fetchCitationByDOIOpenAlex(doi);
    if (jsonObjectFoundString == null) {
      return references;
    }
    Map<String, Object> jsonObjectFound = new JSONObject(jsonObjectFoundString).toMap();
    Reference referenceFound =
        new Reference(jsonObjectFound, id, ReferenceSource.OPENALEX, bibTexPagePreprocessing);
    references.add(referenceFound);
    return references;
  }
}
