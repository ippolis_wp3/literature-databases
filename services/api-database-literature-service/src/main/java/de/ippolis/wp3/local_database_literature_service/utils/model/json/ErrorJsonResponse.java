package de.ippolis.wp3.local_database_literature_service.utils.model.json;

/**
 * JSON response representing an error and containing an error message
 * <p>
 * Implementation of the JSend specification. See https://github.com/omniti-labs/jsend for details.
 */
public class ErrorJsonResponse extends AbstractJsonResponse {

  private String message;

  public ErrorJsonResponse(String message) {
    super(JsonResponseStatus.ERROR);
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
