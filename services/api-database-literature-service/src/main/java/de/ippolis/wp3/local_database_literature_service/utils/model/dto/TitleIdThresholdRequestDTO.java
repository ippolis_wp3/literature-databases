package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

public class TitleIdThresholdRequestDTO {

  private String title;
  private String id;
  private float threshold;

  public TitleIdThresholdRequestDTO() {
  }

  public TitleIdThresholdRequestDTO(String title, String id, float threshold) {
    this.title = title;
    this.id = id;
    this.threshold = threshold;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public float getThreshold() {
    return threshold;
  }

  public void setThreshold(float threshold) {
    this.threshold = threshold;
  }
}
