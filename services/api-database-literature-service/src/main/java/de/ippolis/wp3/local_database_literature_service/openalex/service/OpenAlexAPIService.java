package de.ippolis.wp3.local_database_literature_service.openalex.service;

import de.ippolis.wp3.local_database_literature_service.utils.helper.BibTexReferenceComparisonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class OpenAlexAPIService {

  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;
  private final RawRestServiceOpenAlex restService;

  @Autowired
  public OpenAlexAPIService(
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      RawRestServiceOpenAlex restService) {
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
    this.restService = restService;
  }

  private String preprocessDOI(String doi) {
    if (doi.contains("doi.org")) {
      int indexPrefix = doi.lastIndexOf("doi.org/");
      doi = doi.substring(indexPrefix + 8);
    }
    return doi;
  }

  @Cacheable(value = "openAlexTitle", unless = "#result==null")
  public String fetchCitationByTitleOpenAlex(String title) {
    if (title.equals("")) {
      return null;
    }
    String url =
        "https://api.openalex.org/works?search="
            + bibTexReferenceComparisonHelper.preProcessName(title);
    try {
      final String response = restService.performJsonGetRequest(url);
      return (response);
    } catch (Exception e) {
      return (null);
    }
  }

  @Cacheable(value = "openAlexDOI", unless = "#result==null")
  public String fetchCitationByDOIOpenAlex(String doi) {

    doi = preprocessDOI(doi);

    String url = "https://api.openalex.org/works/doi=" + doi;
    try {
      final String response = restService.performJsonGetRequest(url);
      return (response);
    } catch (Exception e) {
      return (null);
    }
  }
}
