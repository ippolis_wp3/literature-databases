package de.ippolis.wp3.local_database_literature_service.cross_cite.service;

import de.ippolis.wp3.local_database_literature_service.utils.helper.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import de.ippolis.wp3.local_database_literature_service.utils.model.ReferenceSource;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrossCiteAPIReferenceService {

  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;
  private final CrossCiteAPIService crossCiteAPIService;

  @Autowired
  public CrossCiteAPIReferenceService(
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      CrossCiteAPIService crossCiteAPIService) {
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
    this.crossCiteAPIService = crossCiteAPIService;
  }

  public List<Reference> getInfoCrossCiteByDOI(
      String doi, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    List<Reference> referenceList = new LinkedList<>();
    doi = bibTexReferenceComparisonHelper.preprocessDOI(doi);
    doi = bibTexReferenceComparisonHelper.preProcessName(doi);
    String jsonObjectFound = crossCiteAPIService.fetchCitationCrossCite(doi);
    if (jsonObjectFound == null) {
      return referenceList;
    }
    JSONObject jsonObject = new JSONObject(jsonObjectFound);
    if (jsonObject == null) {
      return referenceList;
    }
    Map<String, Object> map = jsonObject.toMap();
    Reference ref = new Reference(map, id, ReferenceSource.CROSS_CITE, bibTexPagePreprocessing);
    referenceList.add(ref);
    return referenceList;
  }

  public List<Reference> getInfoCrossRefByTitle(
      String title, BibTexPagePreprocessing bibTexPagePreprocessing, String entryKey) {
    List<Reference> referenceList = new ArrayList<>();

    title = bibTexReferenceComparisonHelper.preProcessName(title);
    String jsonObjectFoundString = crossCiteAPIService.fetchCitationCrossRef(title);
    if (jsonObjectFoundString == null) {
      return referenceList;
    }
    Map<String, Object> jsonObjectFound = new JSONObject(jsonObjectFoundString).toMap();
    if (jsonObjectFound == null) {
      return referenceList;
    }
    if (jsonObjectFound.containsKey("message")) {
      Map<String, Object> messageMap = (Map<String, Object>) jsonObjectFound.get("message");
      if (messageMap.containsKey("items")) {
        ArrayList<Map<String, Object>> items =
            (ArrayList<Map<String, Object>>) messageMap.get("items");
        for (Map<String, Object> item : items) {
          Reference ref =
              new Reference(item, entryKey, ReferenceSource.CROSS_CITE, bibTexPagePreprocessing);
          referenceList.add(ref);
        }
      }
    }
    return referenceList;
  }
}
