package de.ippolis.wp3.local_database_literature_service.dblp.controller;

import de.ippolis.wp3.local_database_literature_service.dblp.service.DBLPAPIReferenceService;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import de.ippolis.wp3.local_database_literature_service.utils.model.dto.TitleIdBibTexPagePreprocessingRequestDTO;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.local_database_literature_service.utils.model.json.SuccessJsonResponse;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DBLPController {

  private final DBLPAPIReferenceService dblpapiReferenceService;
  Logger logger = LoggerFactory.getLogger(DBLPController.class);

  @Autowired
  public DBLPController(
      DBLPAPIReferenceService dblpapiReferenceService) {

    this.dblpapiReferenceService = dblpapiReferenceService;
  }

  @PostMapping(
      value = "/dblp/getAPIReferencesByTitle",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesCrossCiteByTitle(
      @RequestBody TitleIdBibTexPagePreprocessingRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      String id = requestDTO.getId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          dblpapiReferenceService.getDBLPReferencesByTitle(title, bibTexPagePreprocessing, id);
      logger.info("Response data: {}", results);
      SuccessJsonResponse response = new SuccessJsonResponse(results);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

}
