package de.ippolis.wp3.local_database_literature_service.semantic_scholar.service;

import de.ippolis.wp3.local_database_literature_service.semantic_scholar.model.SemanticScholarIdentifierTypes;
import de.ippolis.wp3.local_database_literature_service.utils.helper.BibTexReferenceComparisonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class SemanticScholarAPIService {

  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;
  private final RawRestServiceSemanticScholar restService;

  @Autowired
  public SemanticScholarAPIService(
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      RawRestServiceSemanticScholar restService) {
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
    this.restService = restService;
  }

  @Cacheable(value = "semanticScholarTitle", unless = "#result==null")
  public String fetchSemanticScholarIDByTitle(String title) {
    if (title.equals("")) {
      return null;
    }
    String basicUrl = "https://api.semanticscholar.org/graph/v1/paper/search?query=";
    String parameters =
        "&fields=title,venue,externalIds,year,referenceCount,citationCount,influentialCitationCount,isOpenAccess,fieldsOfStudy,s2FieldsOfStudy,publicationTypes,publicationDate,journal,authors,abstract";
    String url = "";
    String titlePre = bibTexReferenceComparisonHelper.preProcessName(title);
    url = basicUrl + titlePre + parameters;
    try {
      final String response = restService.performJsonGetRequest(url);
      return (response);
    } catch (Exception e) {
      return (null);
    }
  }

  @Cacheable(value = "semanticScholarId", unless = "#result==null")
  public String fetchCitationSemanticScholar(
      String identifier, SemanticScholarIdentifierTypes identifierType) {
    if (identifier.equals("")) {
      return null;
    }
    String basicUrl = "https://api.semanticscholar.org/graph/v1/paper/";
    String parameters =
        "?fields=title,venue,externalIds,year,referenceCount,citationCount,influentialCitationCount,isOpenAccess,fieldsOfStudy,s2FieldsOfStudy,publicationTypes,publicationDate,journal,authors,abstract";
    String url = "";
    switch (identifierType) {
      case SEMANTIC_SCHOLAR_ID:
        url = basicUrl + identifier + parameters;
        break;
      case DOI:
        url = basicUrl + "DOI:" + identifier + parameters;
        break;
      case CORPUSID:
        url = basicUrl + "CorpusID:" + identifier + parameters;
        break;
      case ARXIV:
        url = basicUrl + "ARXIV:" + identifier + parameters;
        break;
      case MAG:
        url = basicUrl + "MAG:" + identifier + parameters;
        break;
      case ACL:
        url = basicUrl + "ACL:" + identifier + parameters;
        break;
      case PMID:
        url = basicUrl + "PMID:" + identifier + parameters;
        break;
      case PMCID:
        url = basicUrl + "PMCID:" + identifier + parameters;
        break;
      case URL:
        url = basicUrl + "URL:" + identifier + parameters;
        break;
    }
    try {
      final String response = restService.performJsonGetRequest(url);
      return (response);
    } catch (Exception e) {
      return (null);
    }
  }
}
