package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;

public class TitleBibTexPagePreprocessingRequestDTO {

  private String title;
  private String id;
  private BibTexPagePreprocessing bibTexPagePreprocessing;

  public TitleBibTexPagePreprocessingRequestDTO() {
  }

  public TitleBibTexPagePreprocessingRequestDTO(String title,
      BibTexPagePreprocessing bibTexPagePreprocessing, String id) {
    this.title = title;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
