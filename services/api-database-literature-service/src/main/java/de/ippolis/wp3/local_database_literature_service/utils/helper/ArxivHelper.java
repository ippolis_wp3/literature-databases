package de.ippolis.wp3.local_database_literature_service.utils.helper;

import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ArxivHelper {

  private final TextHelperService textHelperService;
  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;

  @Autowired
  public ArxivHelper(
      TextHelperService textHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper) {
    this.textHelperService = textHelperService;
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
  }

  public String removeArxivPrefix(String id) {
    if (id.toLowerCase().contains("arxiv:")) {
      int index = id.toLowerCase().indexOf("arxiv:");
      id = id.substring(index + 6);
    }
    if (id.toLowerCase().contains("abs/")) {
      int index = id.toLowerCase().indexOf("abs/");
      id = id.substring(index + 4);
    }
    return id;
  }

  public String removeVersionFromArxivId(String id) {
    if (id.length() >= 2) {
      if (id.substring(id.length() - 2).toLowerCase().startsWith("v")) {
        id = id.substring(0, id.length() - 2);
      }
    }
    return id;
  }

  public boolean checkArxivInText(String text) {
    if (text.toLowerCase().contains("corr")) {
      return true;
    }
    if (text.toLowerCase().contains("arxiv")) {
      return true;
    }
    return text.toLowerCase().contains("computing research repository");
  }

  private String extractArxivIdFromString(String text) {
    String resultingId = extractArxivIdNumericFromString(text);
    if (resultingId.strip().equals("")) {
      resultingId = extractArxivIdOldFromString(text);
      return resultingId;
    } else {
      return resultingId;
    }
  }

  public List<Reference> filterFoundArxivReferences(
      List<Reference> foundReferences,
      String arxivId,
      String title,
      List<String> authors,
      String doi) {
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : foundReferences) {
      if (!reference.isArxiv(this)) {
        if (!referenceHasNoVenue(reference)) {
          if (!referenceWithoutOrMiscPublicationType(reference)) {
            if (!doi.equals("")) {
              String doiFound = reference.getDoi();
              if (doiFound == null || doiFound.equals("")) {
                String titleFound = reference.getTitleCorrect();
                if (bibTexReferenceComparisonHelper.checkTitlesMatch(title, titleFound)) {
                  if (bibTexReferenceComparisonHelper.checkAuthorsMatch(
                      reference.getSurnames(), authors)) {
                    filteredList.add(reference);
                  }
                }

              } else {
                if (bibTexReferenceComparisonHelper
                    .preprocessDOI(doiFound)
                    .contains(bibTexReferenceComparisonHelper.preprocessDOI(doi))) {
                  filteredList.add(reference);
                }
              }
            } else {
              String titleFound = reference.getTitleCorrect();
              if (bibTexReferenceComparisonHelper.checkTitlesMatch(title, titleFound)) {
                if (bibTexReferenceComparisonHelper.checkAuthorsMatch(
                    reference.getSurnames(), authors)) {
                  filteredList.add(reference);
                }
              }
            }
          }
        }
      }
    }
    return filteredList;
  }

  private boolean referenceWithoutOrMiscPublicationType(Reference reference) {
    if (reference.getPublicationType() == null) {
      return true;
    }
    if (reference.getPublicationType().strip().equals("")) {
      return true;
    }
    return reference.getPublicationType().toLowerCase().strip().equalsIgnoreCase("misc");
  }

  private boolean referenceHasNoVenue(Reference ref) {
    if (ref.getJournal() == null) {
      return true;
    }
    if (ref.getJournal().equals("")) {
      return true;
    }
    return false;
  }

  public List<Reference> filterFoundArxivReferencesByArxivId(
      List<Reference> foundReferences, String doi) {
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : foundReferences) {
      if (!reference.isArxiv(this)) {
        if (!referenceHasNoVenue(reference)) {
          if (!referenceWithoutOrMiscPublicationType(reference)) {
            if (doi.equals("")) {
              filteredList.add(reference);
            } else {
              String doiFound = reference.getDoi();
              if (doiFound == null || doiFound.equals("")) {
                filteredList.add(reference);
              } else {
                if (bibTexReferenceComparisonHelper
                    .preprocessDOI(doiFound)
                    .contains(bibTexReferenceComparisonHelper.preprocessDOI(doi))) {
                  filteredList.add(reference);
                }
              }
            }
          }
        }
      }
    }
    return filteredList;
  }

  private String extractArxivIdOldFromString(String text) {
    String patternStr = "[a-z]+-[a-z]+/[0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
    List<TextRange> indexesOfPatternInText =
        textHelperService.getIndexesOfPatternInText(text, patternStr);
    for (TextRange textRange : indexesOfPatternInText) {
      int begin = textRange.getStart();
      int end = textRange.getEnd();
      return text.substring(begin, end);
    }
    return "";
  }

  private String extractArxivIdNumericFromString(String text) {
    String patternStr = "[0-9][0-9][0-9][0-9]\\.[0-9][0-9][0-9][0-9]+";
    List<TextRange> indexesOfPatternInText =
        textHelperService.getIndexesOfPatternInText(text, patternStr);

    for (TextRange textRange : indexesOfPatternInText) {
      int begin = textRange.getStart();
      int end = textRange.getEnd();
      return text.substring(begin, end);
    }
    return "";
  }

  private boolean textIsNumeric(String text) {
    if (text == null) {
      return false;
    }
    try {
      double d = Integer.parseInt(text);
    } catch (NumberFormatException nfe) {
      return false;
    }
    return true;
  }

  public String preprocessArxiv(String arxivId) {
    if (arxivId.toLowerCase().contains("arxiv:")) {
      int index = arxivId.toLowerCase().indexOf("arxiv:");
      arxivId = arxivId.substring(index + 6);
    }
    if (arxivId.toLowerCase().contains("abs/")) {
      int index = arxivId.toLowerCase().indexOf("abs/");
      arxivId = arxivId.substring(index + 4);
    }
    if (arxivId.toLowerCase().contains("pdf/")) {
      int index = arxivId.toLowerCase().indexOf("pdf/");
      arxivId = arxivId.substring(index + 4);
    }
    if (arxivId.toLowerCase().endsWith(".pdf")) {
      arxivId = arxivId.substring(0, arxivId.length() - 4);
    }
    if (arxivId.toLowerCase().contains("v")) {
      int index = arxivId.toLowerCase().lastIndexOf("v");
      String substring = arxivId.substring(index + 1);
      if (textIsNumeric(substring)) {
        arxivId = arxivId.substring(index);
      }
    }
    return arxivId.toLowerCase();
  }
}
