package de.ippolis.wp3.local_database_literature_service.dblp.service;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import de.ippolis.wp3.local_database_literature_service.utils.model.ReferenceSource;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class DBLPAPIReferenceService {

  private final DBLPAPIService dblpAPIService;

  public DBLPAPIReferenceService(DBLPAPIService dblpAPIService) {
    this.dblpAPIService = dblpAPIService;
  }

  public List<Reference> getDBLPReferencesByTitle(
      String title, BibTexPagePreprocessing bibTexPagePreprocessing, String id) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    List<Reference> referenceList = new LinkedList<>();
    String jsonObjectString = dblpAPIService.fetchDBLPByTitle(title);
    if (jsonObjectString == null) {
      return referenceList;
    }
    Map<String, Object> jsonObject = new JSONObject(jsonObjectString).toMap();
    if (jsonObject == null) {
      return referenceList;
    }
    if (jsonObject.containsKey("result")) {
      Map<String, Object> resultsMap = (Map<String, Object>) jsonObject.get("result");
      if (resultsMap.containsKey("hits")) {
        Map<String, Object> resultsMapHits = (Map<String, Object>) resultsMap.get("hits");
        if (resultsMapHits.containsKey("hit")) {
          ArrayList<Map<String, Object>> authorsByJSONObject =
              (ArrayList<Map<String, Object>>) resultsMapHits.get("hit");
          for (Map<String, Object> match : authorsByJSONObject) {
            Reference ref = new Reference(match, id, ReferenceSource.DBLP, bibTexPagePreprocessing);
            referenceList.add(ref);
          }
        }
      }
    }
    return referenceList;
  }
}
