package de.ippolis.wp3.local_database_literature_service.semantic_scholar.service;

import de.ippolis.wp3.local_database_literature_service.semantic_scholar.model.SemanticScholarIdentifierTypes;
import de.ippolis.wp3.local_database_literature_service.utils.helper.ArxivHelper;
import de.ippolis.wp3.local_database_literature_service.utils.helper.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import de.ippolis.wp3.local_database_literature_service.utils.model.ReferenceSource;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SemanticScholarAPIReferenceService {

  private final SemanticScholarAPIService semanticScholarAPIService;
  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;
  private final ArxivHelper arxivHelper;

  @Autowired
  public SemanticScholarAPIReferenceService(
      SemanticScholarAPIService semanticScholarAPIService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      ArxivHelper arxivHelper) {
    this.semanticScholarAPIService = semanticScholarAPIService;
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
    this.arxivHelper = arxivHelper;
  }

  public List<Reference> getReferencesByTitle(
      String title, BibTexPagePreprocessing bibtexPagePreprocessing, String id) {
    List<Reference> resultsList = new LinkedList<>();
    title = bibTexReferenceComparisonHelper.preProcessName(title);
    String jsonObjectString = semanticScholarAPIService.fetchSemanticScholarIDByTitle(title);
    if (jsonObjectString == null) {
      return resultsList;
    }
    Map<String, Object> jsonObject = new JSONObject(jsonObjectString).toMap();
    if (jsonObject == null) {
      return resultsList;
    }
    Set<String> keySet = jsonObject.keySet();
    if (keySet.contains("data")) {
      ArrayList<Map<String, Object>> object =
          (ArrayList<Map<String, Object>>) jsonObject.get("data");
      for (Map<String, Object> mapObject : object) {
        Reference reference =
            new Reference(mapObject, id, ReferenceSource.SEMANTIC_SCHOLAR, bibtexPagePreprocessing);
        resultsList.add(reference);
      }
    }
    return resultsList;
  }

  public List<Reference> getReferencesByArxiv(
      String arxivId, BibTexPagePreprocessing bibtexPagePreprocessing) {
    List<Reference> results = new LinkedList<>();
    arxivId = arxivHelper.preprocessArxiv(arxivId);
    String jsonObjectFoundString =
        semanticScholarAPIService.fetchCitationSemanticScholar(
            arxivId, SemanticScholarIdentifierTypes.ARXIV);
    if (jsonObjectFoundString == null) {
      return new LinkedList<>();
    }
    Map<String, Object> jsonObjectFound = new JSONObject(jsonObjectFoundString).toMap();
    if (jsonObjectFound == null) {
      return new LinkedList<>();
    }
    Reference reference =
        new Reference(
            jsonObjectFound, arxivId, ReferenceSource.SEMANTIC_SCHOLAR, bibtexPagePreprocessing);
    results.add(reference);
    return results;
  }

  public List<Reference> getReferencesByDOI(
      String doi, BibTexPagePreprocessing bibtexPagePreprocessing, String id) {
    List<Reference> results = new LinkedList<>();
    doi = bibTexReferenceComparisonHelper.preprocessDOI(doi);
    String jsonObjectFoundString =
        semanticScholarAPIService.fetchCitationSemanticScholar(
            doi, SemanticScholarIdentifierTypes.DOI);
    if (jsonObjectFoundString == null) {
      return new LinkedList<>();
    }
    Map<String, Object> jsonObjectFound = new JSONObject(jsonObjectFoundString).toMap();
    if (jsonObjectFound == null) {
      return new LinkedList<>();
    }
    Reference reference =
        new Reference(
            jsonObjectFound, id, ReferenceSource.SEMANTIC_SCHOLAR, bibtexPagePreprocessing);
    results.add(reference);
    return results;
  }
}
