package de.ippolis.wp3.local_database_literature_service.dblp.repository;

import de.ippolis.wp3.local_database_literature_service.dblp.model.DBLPReferenceDocument;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBLPElasticSearchReferenceRepository extends
    ElasticsearchRepository<DBLPReferenceDocument, String> {

  List<DBLPReferenceDocument> findByDoi(String doi);

  List<DBLPReferenceDocument> findByArxiv(String arxiv);

  List<DBLPReferenceDocument> findByTitle(String title);
}
