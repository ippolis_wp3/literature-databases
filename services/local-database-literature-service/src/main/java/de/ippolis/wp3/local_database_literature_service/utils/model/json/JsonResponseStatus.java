package de.ippolis.wp3.local_database_literature_service.utils.model.json;

/**
 * JSON response status codes: SUCCESS, FAIL, and ERROR
 * <p>
 * Implementation of the JSend specification. See https://github.com/omniti-labs/jsend for details.
 * <p>
 * We use capital letters because the JSON converter for Enums ignores the values and uses the Enum
 * names.
 */
public enum JsonResponseStatus {
  SUCCESS("SUCCESS"), FAIL("FAIL"), ERROR("ERROR");

  private final String statusMessage;

  JsonResponseStatus(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  @Override
  public String toString() {
    return this.statusMessage;
  }
}
