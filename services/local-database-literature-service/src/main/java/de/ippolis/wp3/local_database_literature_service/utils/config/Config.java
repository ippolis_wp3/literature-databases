package de.ippolis.wp3.local_database_literature_service.utils.config;

import de.ippolis.wp3.local_database_literature_service.utils.helper.ResourceHelperService;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages
    = {"de.ippolis.wp3.local_database_literature_service.openalex.repository",
    "de.ippolis.wp3.local_database_literature_service.dblp.repository",
    "de.ippolis.wp3.local_database_literature_service.cross_cite.repository"})
@ComponentScan(basePackages = {"de.ippolis.wp3.local_database_literature_service.openalex",
    "de.ippolis.wp3.local_database_literature_service.dblp",
    "de.ippolis.wp3.local_database_literature_service.cross_cite"})
public class Config extends
    AbstractElasticsearchConfiguration {

  private final Resource elasticSearchConfigFile;
  private final ResourceHelperService resourceHelperService;

  @Autowired
  public Config(
      @Value("classpath:secret/elasticSearchConfig.txt") Resource elasticSearchConfigFile,
      ResourceHelperService resourceHelperService) {
    this.elasticSearchConfigFile = elasticSearchConfigFile;
    this.resourceHelperService = resourceHelperService;
  }

  @Override
  @Bean
  public RestHighLevelClient elasticsearchClient() {
    String url = "";
    String username = "";
    String password = "";
    if (elasticSearchConfigFile.exists()) {
      List<String> lines = null;
      try {
        lines = this.resourceHelperService.readLinesFromResource(elasticSearchConfigFile);
      } catch (IOException e) {
        e.printStackTrace();
      }
      if (lines != null) {
        if (lines.size() == 3) {
          url = lines.get(0);
          username = lines.get(1);
          password = lines.get(2);
        }
      }
    }
    if (url.strip().equals("")) {
      return null;
    } else {
      final ClientConfiguration clientConfiguration =
          ClientConfiguration
              .builder()
              .connectedTo(url).withBasicAuth(username, password)
              .withConnectTimeout(Duration.ofSeconds(20)).withSocketTimeout(Duration.ofSeconds(20))
              .build();

      return RestClients.create(clientConfiguration).rest();
    }
  }
}
