package de.ippolis.wp3.local_database_literature_service.cross_cite.repository;

import de.ippolis.wp3.local_database_literature_service.cross_cite.model.CrossCiteReferenceDocument;
import java.util.List;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrossCiteElasticSearchReferenceRepository
    extends ElasticsearchRepository<CrossCiteReferenceDocument, String> {

  List<CrossCiteReferenceDocument> findByDoi(String doi);

  List<CrossCiteReferenceDocument> findByTitle(String title);
}
