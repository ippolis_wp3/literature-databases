package de.ippolis.wp3.local_database_literature_service.cross_cite.service;

import de.ippolis.wp3.local_database_literature_service.cross_cite.model.CrossCiteReferenceDocument;
import de.ippolis.wp3.local_database_literature_service.cross_cite.repository.CrossCiteElasticSearchReferenceRepository;
import de.ippolis.wp3.local_database_literature_service.cross_cite.repository.CrossRefFuzzyElasticSearchRepository;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrossCiteLocalDatabaseService {

  private final CrossCiteElasticSearchReferenceRepository crossCiteElasticSearchReferenceRepository;
  private final CrossRefFuzzyElasticSearchRepository crossRefFuzzyElasticSearchRepository;

  @Autowired
  public CrossCiteLocalDatabaseService(
      CrossCiteElasticSearchReferenceRepository crossCiteElasticSearchReferenceRepository,
      CrossRefFuzzyElasticSearchRepository crossRefFuzzyElasticSearchRepository) {
    this.crossCiteElasticSearchReferenceRepository = crossCiteElasticSearchReferenceRepository;

    this.crossRefFuzzyElasticSearchRepository = crossRefFuzzyElasticSearchRepository;
  }

  private String preprocessTitle(String title) {
    title = title.replaceAll("[^a-zA-Z0-9\s]", "");
    return title;
  }

  public List<Reference> getByTitle(String title, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    title = preprocessTitle(title);
    if (title.strip().equals("")) {
      return new LinkedList<>();
    }

    List<CrossCiteReferenceDocument> resultsByTitle =
        crossCiteElasticSearchReferenceRepository.findByTitle(title);
    if (resultsByTitle.size() <= 0) {
      resultsByTitle = crossRefFuzzyElasticSearchRepository.findByTitleFuzzy(title);
    }
    List<Reference> references = new LinkedList<>();
    for (CrossCiteReferenceDocument crossCiteReferenceEntity : resultsByTitle) {
      Reference reference =
          convertCrossCiteReferenceEntityToReference(crossCiteReferenceEntity, id,
              bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  public List<Reference> getByDOI(String doi, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi.equals("")) {
      return new LinkedList<>();
    }
    List<CrossCiteReferenceDocument> byDoiIgnoreCaseEndsWith =
        crossCiteElasticSearchReferenceRepository.findByDoi(doi);
    List<Reference> references = new LinkedList<>();

    for (CrossCiteReferenceDocument crossCiteReferenceEntity : byDoiIgnoreCaseEndsWith) {
      Reference reference =
          convertCrossCiteReferenceEntityToReference(crossCiteReferenceEntity, id,
              bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  private Reference convertCrossCiteReferenceEntityToReference(
      CrossCiteReferenceDocument crossCiteReferenceEntity, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    Reference reference = new Reference(id);
    if (crossCiteReferenceEntity.getJournal() != null) {
      reference.setJournal(crossCiteReferenceEntity.getJournal());
    }
    if (crossCiteReferenceEntity.getAuthors() != null) {
      String[] authorsIdentified = crossCiteReferenceEntity.getAuthors().split(";");
      for (String authorIdentified : authorsIdentified) {
        if (!authorIdentified.strip().equals("")) {
          reference.addAuthor(authorIdentified);
        }
      }
    }

    if (crossCiteReferenceEntity.getDoi() != null) {
      String doiFound = crossCiteReferenceEntity.getDoi();
      if (doiFound.toLowerCase().contains("doi.org/")) {
        int index = doiFound.lastIndexOf("doi.org/");
        doiFound = doiFound.substring(index + 8);
      }
      doiFound = doiFound.toLowerCase();
      reference.setDoi(doiFound);
    }
    if (crossCiteReferenceEntity.getEditors() != null) {
      String[] authorsIdentified = crossCiteReferenceEntity.getEditors().split(";");
      for (String authorIdentified : authorsIdentified) {
        if (!authorIdentified.strip().equals("")) {
          reference.addEditor(authorIdentified);
        }
      }
    }
    if (crossCiteReferenceEntity.getIsbn() != null) {
      reference.setISBN(crossCiteReferenceEntity.getIsbn());
    }
    if (crossCiteReferenceEntity.getIssue() != null) {
      reference.setIssue(crossCiteReferenceEntity.getIssue());
    }
    if (crossCiteReferenceEntity.getPage() != null) {
      reference.setPages(crossCiteReferenceEntity.getPage(), bibTexPagePreprocessing);
    }
    if (crossCiteReferenceEntity.getPublication_type() != null) {

      String type = crossCiteReferenceEntity.getPublication_type();
      if (type.equalsIgnoreCase("book-part")
          || type.equalsIgnoreCase("proceedings-article")
          || type.equalsIgnoreCase("book-chapter")) {
        reference.setPublicationType("inproceedings");
      } else {
        if (type.equalsIgnoreCase("journal") || type.equalsIgnoreCase("journal-article")) {
          reference.setPublicationType("article");
        } else {
          if (type.equalsIgnoreCase("book")
              || type.equalsIgnoreCase("monograph")
              || type.equalsIgnoreCase("edited-book")) {
            reference.setPublicationType("book");
          } else {
            if (type.equalsIgnoreCase("proceedings") || type.equalsIgnoreCase("report-series")) {
              reference.setPublicationType("proceedings");
            }
            reference.setPublicationType("misc");
          }
        }
      }
    }
    if (crossCiteReferenceEntity.getPublisher() != null) {
      reference.setPublisher(crossCiteReferenceEntity.getPublisher());
    }
    if (crossCiteReferenceEntity.getTitle() != null) {
      reference.setTitleCorrect(crossCiteReferenceEntity.getTitle());
    }
    if (crossCiteReferenceEntity.getVolume() != null) {
      reference.setVolume(crossCiteReferenceEntity.getVolume());
    }
    if (crossCiteReferenceEntity.getYear() != null) {
      reference.setYear(crossCiteReferenceEntity.getYear());
    }
    if (crossCiteReferenceEntity.getLicenses() != null) {
      String[] split = crossCiteReferenceEntity.getLicenses().split(";");
      for (String license : split) {
        reference.addLicense(license.strip());
      }
    }
    if (crossCiteReferenceEntity.getIssn() != null) {
      reference.setISSN(crossCiteReferenceEntity.getIssn());
    }
    if (crossCiteReferenceEntity.getPublisher_location() != null) {
      reference.setPublisherLocation(crossCiteReferenceEntity.getPublisher_location());
    }
    if (crossCiteReferenceEntity.getCitationCount() != -1) {
      reference.setCitationCount(crossCiteReferenceEntity.getCitationCount());
    }
    if (crossCiteReferenceEntity.getPublication_date() != null) {
      reference.setPublicationDate(crossCiteReferenceEntity.getPublication_type());
    }
    return reference;
  }
}
