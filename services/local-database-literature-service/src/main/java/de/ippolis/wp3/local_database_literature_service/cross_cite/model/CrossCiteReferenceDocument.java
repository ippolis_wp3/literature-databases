package de.ippolis.wp3.local_database_literature_service.cross_cite.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Document(indexName = "crossrefidx")
public class CrossCiteReferenceDocument {

  private String authors;
  private String editors;
  private String licenses;
  @Id
  private String id;
  private String title;
  private String year;
  private String journal;
  private String volume;
  private String page;
  private String doi;
  private String publication_type;
  private String issue;
  private String issn;
  private String isbn;
  private String publisher;
  private String publisher_location;
  private int citationCount;
  private String abstract_found;
  private String publication_date;

  public CrossCiteReferenceDocument() {
  }

  public CrossCiteReferenceDocument(
      String authors,
      String editors,
      String licenses,
      String id,
      String title,
      String year,
      String journal,
      String volume,
      String page,
      String doi,
      String publication_type,
      String issue,
      String issn,
      String isbn,
      String publisher,
      String publisher_location,
      int citationCount,
      String abstract_found,
      String publication_date) {
    this.authors = authors;
    this.editors = editors;
    this.licenses = licenses;
    this.id = id;
    this.title = title;
    this.year = year;
    this.journal = journal;
    this.volume = volume;
    this.page = page;
    this.doi = doi;
    this.publication_type = publication_type;
    this.issue = issue;
    this.issn = issn;
    this.isbn = isbn;
    this.publisher = publisher;
    this.publisher_location = publisher_location;
    this.citationCount = citationCount;
    this.abstract_found = abstract_found;
    this.publication_date = publication_date;
  }

  public String getAuthors() {
    return authors;
  }

  public void setAuthors(String authors) {
    this.authors = authors;
  }

  public String getEditors() {
    return editors;
  }

  public void setEditors(String editors) {
    this.editors = editors;
  }

  public String getLicenses() {
    return licenses;
  }

  public void setLicenses(String licenses) {
    this.licenses = licenses;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getJournal() {
    return journal;
  }

  public void setJournal(String journal) {
    this.journal = journal;
  }

  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume = volume;
  }

  public String getPage() {
    return page;
  }

  public void setPage(String page) {
    this.page = page;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getPublication_type() {
    return publication_type;
  }

  public void setPublication_type(String publication_type) {
    this.publication_type = publication_type;
  }

  public String getIssue() {
    return issue;
  }

  public void setIssue(String issue) {
    this.issue = issue;
  }

  public String getIssn() {
    return issn;
  }

  public void setIssn(String issn) {
    this.issn = issn;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public String getPublisher_location() {
    return publisher_location;
  }

  public void setPublisher_location(String publisher_location) {
    this.publisher_location = publisher_location;
  }

  public int getCitationCount() {
    return citationCount;
  }

  public void setCitationcount(int citationCount) {
    this.citationCount = citationCount;
  }

  public String getAbstract_found() {
    return abstract_found;
  }

  public void setAbstract_found(String abstract_found) {
    this.abstract_found = abstract_found;
  }

  public String getPublication_date() {
    return publication_date;
  }

  public void setPublication_date(String publication_date) {
    this.publication_date = publication_date;
  }
}
