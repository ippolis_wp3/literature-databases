package de.ippolis.wp3.local_database_literature_service.utils.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * Helper service for handling resources
 */
@Service
public class ResourceHelperService {

  public List<String> readLinesFromResource(Resource resource) throws IOException {
    final InputStream inputStream = resource.getInputStream();
    final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    return bufferedReader.lines().collect(Collectors.toList());
  }

  public byte[] readResourceBytes(Resource resource) throws IOException {
    final InputStream inputStream = resource.getInputStream();
    return inputStream.readAllBytes();
  }
}
