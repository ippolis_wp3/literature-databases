package de.ippolis.wp3.local_database_literature_service.openalex.repository;

import de.ippolis.wp3.local_database_literature_service.openalex.model.OpenAlexReferenceDocument;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Repository;

@Repository
public class OpenAlexFuzzyElasticSearchRepository {

  private final RestHighLevelClient elasticsearchClient;

  public OpenAlexFuzzyElasticSearchRepository(
      RestHighLevelClient elasticsearchClient) {

    this.elasticsearchClient = elasticsearchClient;
  }

  public List<OpenAlexReferenceDocument> findByTitleFuzzy(String title) {
    MatchQueryBuilder fuzzyQuery = QueryBuilders.matchQuery("title", title)
        .fuzziness(0).fuzzyTranspositions(true).prefixLength(10).maxExpansions(5);
    BoolQueryBuilder bool = new BoolQueryBuilder();

    bool.should(fuzzyQuery);

    SearchSourceBuilder search = new SearchSourceBuilder();

    search.query(bool);

    SearchRequest searchRequest = new SearchRequest("openalexidx");

    searchRequest.source(search);

    try {
      SearchResponse res = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);
      SearchHit[] hits = res.getHits().getHits();
      List<OpenAlexReferenceDocument> results = new LinkedList<>();
      for (SearchHit hit : hits) {
        OpenAlexReferenceDocument yourObject = new com.fasterxml.jackson.databind.ObjectMapper().convertValue(
            hit.getSourceAsMap(), OpenAlexReferenceDocument.class);
        results.add(yourObject);
      }
      return results;
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new LinkedList<>();
  }
}
