package de.ippolis.wp3.local_database_literature_service.dblp.service;

import de.ippolis.wp3.local_database_literature_service.dblp.model.DBLPReferenceDocument;
import de.ippolis.wp3.local_database_literature_service.dblp.repository.DBLPElasticSearchReferenceRepository;
import de.ippolis.wp3.local_database_literature_service.dblp.repository.DBLPFuzzyElasticSearchReferenceRepository;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DBLPLocalDataBaseService {

  private final DBLPElasticSearchReferenceRepository dblpElasticSearchReferenceRepository;
  private final DBLPFuzzyElasticSearchReferenceRepository dblpFuzzyElasticSearchReferenceRepository;

  @Autowired
  public DBLPLocalDataBaseService(
      DBLPElasticSearchReferenceRepository dblpElasticSearchReferenceRepository,
      DBLPFuzzyElasticSearchReferenceRepository dblpFuzzyElasticSearchReferenceRepository) {
    this.dblpElasticSearchReferenceRepository = dblpElasticSearchReferenceRepository;
    this.dblpFuzzyElasticSearchReferenceRepository = dblpFuzzyElasticSearchReferenceRepository;
  }

  private String preprocessTitle(String title) {
    title = title.replaceAll("[^a-zA-Z0-9]", " ");
    return title;
  }

  public List<Reference> getByTitle(String title, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    title = preprocessTitle(title);
    if (title.strip().equals("")) {
      return new LinkedList<>();
    }

    List<DBLPReferenceDocument> resultsByTitle =
        dblpElasticSearchReferenceRepository.findByTitle(title);
    if (resultsByTitle.size() <= 0) {
      resultsByTitle = dblpFuzzyElasticSearchReferenceRepository.findByTitleFuzzy(title);
    }
    List<Reference> references = new LinkedList<>();
    for (DBLPReferenceDocument dblpReferenceDTO : resultsByTitle) {
      Reference reference = convertDBLPReferenceEntityToReference(dblpReferenceDTO, id,
          bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  public List<Reference> getByArxivId(String arxivId, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (arxivId.equals("")) {
      return new LinkedList<>();
    }
    List<DBLPReferenceDocument> byArxivId =
        dblpElasticSearchReferenceRepository.findByArxiv(arxivId);
    List<Reference> references = new LinkedList<>();
    for (DBLPReferenceDocument dblpReferenceDTO : byArxivId) {
      Reference reference = convertDBLPReferenceEntityToReference(dblpReferenceDTO, id,
          bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  public List<Reference> getByDOI(String doi, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi.equals("")) {
      return new LinkedList<>();
    }
    List<DBLPReferenceDocument> byDoiIgnoreCaseEndsWith =
        dblpElasticSearchReferenceRepository.findByDoi(doi);
    List<Reference> references = new LinkedList<>();

    for (DBLPReferenceDocument dblpReferenceDTO : byDoiIgnoreCaseEndsWith) {
      Reference reference = convertDBLPReferenceEntityToReference(dblpReferenceDTO, id,
          bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  private Reference convertDBLPReferenceEntityToReference(
      DBLPReferenceDocument dblpReferenceDTO, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    Reference reference = new Reference(id);
    if (dblpReferenceDTO.getJournal() != null) {
      reference.setJournal(dblpReferenceDTO.getJournal());
    }
    if (dblpReferenceDTO.getAuthors() != null) {
      String[] authorsIdentified = dblpReferenceDTO.getAuthors().split(";");
      for (String authorIdentified : authorsIdentified) {
        if (!authorIdentified.strip().equals("")) {
          reference.addAuthor(authorIdentified);
        }
      }
    }

    if (dblpReferenceDTO.getDoi() != null) {
      String doiFound = dblpReferenceDTO.getDoi();
      if (doiFound.toLowerCase().contains("doi.org/")) {
        int index = doiFound.lastIndexOf("doi.org/");
        doiFound = doiFound.substring(index + 8);
      }
      doiFound = doiFound.toLowerCase();
      reference.setDoi(doiFound);
    }
    if (dblpReferenceDTO.getEditors() != null) {
      String[] authorsIdentified = dblpReferenceDTO.getEditors().split(";");
      for (String authorIdentified : authorsIdentified) {
        if (!authorIdentified.strip().equals("")) {
          reference.addEditor(authorIdentified);
        }
      }
    }
    if (dblpReferenceDTO.getIsbn() != null) {
      reference.setISBN(dblpReferenceDTO.getIsbn());
    }
    if (dblpReferenceDTO.getIssue() != null) {
      reference.setIssue(dblpReferenceDTO.getIssue());
    }
    if (dblpReferenceDTO.getPages() != null) {
      reference.setPages(dblpReferenceDTO.getPages(), bibTexPagePreprocessing);
    }
    if (dblpReferenceDTO.getPublication_type() != null) {

      String type = dblpReferenceDTO.getPublication_type();
      if (type.equalsIgnoreCase("Conference")
          || type.equalsIgnoreCase("Conference and Workshop Papers")
          || type.equalsIgnoreCase("inproceedings")) {
        reference.setPublicationType("inproceedings");
      } else {
        if (type.equalsIgnoreCase("JournalArticle")
            || type.equalsIgnoreCase("Journal Articles")
            || type.equalsIgnoreCase("article")) {
          reference.setPublicationType("article");
        } else {
          if (type.equalsIgnoreCase("Books and Theses")
              || type.equalsIgnoreCase("Editorship")
              || type.equalsIgnoreCase("book")) {
            reference.setPublicationType("book");
          } else {
            if (type.equalsIgnoreCase("proceedings")) {
              reference.setPublicationType("proceedings");
            }
            reference.setPublicationType("misc");
          }
        }
      }
    }
    if (dblpReferenceDTO.getPublisher() != null) {
      reference.setPublisher(dblpReferenceDTO.getPublisher());
    }
    if (dblpReferenceDTO.getTitle() != null) {
      if (dblpReferenceDTO.getTitle().endsWith(".")) {
        reference.setTitleCorrect(
            dblpReferenceDTO.getTitle().substring(0, dblpReferenceDTO.getTitle().length() - 1));
      } else {
        reference.setTitleCorrect(dblpReferenceDTO.getTitle());
      }
    }
    if (dblpReferenceDTO.getVolume() != null) {
      reference.setVolume(dblpReferenceDTO.getVolume());
    }
    if (dblpReferenceDTO.getYear() != null) {
      reference.setYear(dblpReferenceDTO.getYear());
    }
    if (dblpReferenceDTO.getArxiv() != null) {
      reference.setLinkedArxivId(dblpReferenceDTO.getArxiv());
    }
    return reference;
  }
}
