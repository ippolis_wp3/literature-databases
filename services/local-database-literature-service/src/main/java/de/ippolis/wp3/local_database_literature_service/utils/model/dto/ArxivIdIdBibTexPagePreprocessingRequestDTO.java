package de.ippolis.wp3.local_database_literature_service.utils.model.dto;

import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;

public class ArxivIdIdBibTexPagePreprocessingRequestDTO {

  private String arxivId;
  private String id;
  private BibTexPagePreprocessing bibTexPagePreprocessing;

  public ArxivIdIdBibTexPagePreprocessingRequestDTO() {
  }

  public ArxivIdIdBibTexPagePreprocessingRequestDTO(String arxivId, String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.arxivId = arxivId;
    this.id = id;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public String getArxivId() {
    return arxivId;
  }

  public void setArxivId(String arxivId) {
    this.arxivId = arxivId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
