package de.ippolis.wp3.local_database_literature_service.openalex.service;

import de.ippolis.wp3.local_database_literature_service.openalex.model.OpenAlexReferenceDocument;
import de.ippolis.wp3.local_database_literature_service.openalex.repository.OpenAlexElasticSearchRepository;
import de.ippolis.wp3.local_database_literature_service.openalex.repository.OpenAlexFuzzyElasticSearchRepository;
import de.ippolis.wp3.local_database_literature_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.local_database_literature_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OpenAlexLocalDatabaseService {

  private final OpenAlexElasticSearchRepository openAlexReferenceRepository;
  private final OpenAlexFuzzyElasticSearchRepository openAlexFuzzyElasticSearchRepository;

  @Autowired
  public OpenAlexLocalDatabaseService(
      OpenAlexElasticSearchRepository openAlexReferenceRepository,
      OpenAlexFuzzyElasticSearchRepository openAlexFuzzyElasticSearchRepository) {

    this.openAlexReferenceRepository = openAlexReferenceRepository;
    this.openAlexFuzzyElasticSearchRepository = openAlexFuzzyElasticSearchRepository;
  }

  private String preprocessTitle(String title) {
    title = title.replaceAll("[^a-zA-Z0-9]", " ");
    return title;
  }

  public List<Reference> getByTitle(
      String title, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    title = preprocessTitle(title);
    if (title.strip().equals("")) {
      return new LinkedList<>();
    }

    List<OpenAlexReferenceDocument> resultsByTitle =
        openAlexReferenceRepository.findByTitle(title);
    if (resultsByTitle.size() <= 0) {
      resultsByTitle = openAlexFuzzyElasticSearchRepository.findByTitleFuzzy(title);
    }
    List<Reference> references = new LinkedList<>();
    for (OpenAlexReferenceDocument openAlexReferenceDTO : resultsByTitle) {
      Reference reference =
          convertOpenalexReferenceEntityToReference(
              openAlexReferenceDTO, id, bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  public List<Reference> getByArxivId(
      String arxivId, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (arxivId.equals("")) {
      return new LinkedList<>();
    }
    List<OpenAlexReferenceDocument> byArxivId =
        openAlexReferenceRepository.findByArxiv(arxivId);
    List<Reference> references = new LinkedList<>();
    for (OpenAlexReferenceDocument openAlexReferenceEntity : byArxivId) {
      Reference reference =
          convertOpenalexReferenceEntityToReference(
              openAlexReferenceEntity, id, bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  public List<Reference> getByDOI(
      String doi, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi.equals("")) {
      return new LinkedList<>();
    }
    List<OpenAlexReferenceDocument> byDoiIgnoreCaseEndsWith =
        openAlexReferenceRepository.findByDoi(doi);
    List<Reference> references = new LinkedList<>();

    for (OpenAlexReferenceDocument openAlexReferenceEntity : byDoiIgnoreCaseEndsWith) {
      Reference reference =
          convertOpenalexReferenceEntityToReference(
              openAlexReferenceEntity, id, bibTexPagePreprocessing);
      references.add(reference);
    }
    return references;
  }

  private Reference convertOpenalexReferenceEntityToReference(
      OpenAlexReferenceDocument openAlexReferenceDocument,
      String id,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    Reference reference = new Reference(id);
    if (openAlexReferenceDocument.getJournal() != null) {
      reference.setJournal(openAlexReferenceDocument.getJournal());
    }
    if (openAlexReferenceDocument.getAuthors() != null) {
      String[] authorsIdentified = openAlexReferenceDocument.getAuthors().split(";");
      for (String authorIdentified : authorsIdentified) {
        if (!authorIdentified.strip().equals("")) {
          reference.addAuthor(authorIdentified);
        }
      }
    }
    if (openAlexReferenceDocument.getLicenses() != null) {
      String[] licensesIdentified = openAlexReferenceDocument.getLicenses().split(";");
      for (String license : licensesIdentified) {
        if (!license.strip().equals("")) {
          reference.addLicense(license.strip());
        }
      }
    }

    if (openAlexReferenceDocument.getDoi() != null) {
      String doiFound = openAlexReferenceDocument.getDoi();
      if (doiFound.toLowerCase().contains("doi.org/")) {
        int index = doiFound.lastIndexOf("doi.org/");
        doiFound = doiFound.substring(index + 8);
      }
      doiFound = doiFound.toLowerCase();
      reference.setDoi(doiFound);
    }

    if (openAlexReferenceDocument.getNumber() != null) {
      reference.setIssue(openAlexReferenceDocument.getNumber());
    }

    String firstPageExtracted = openAlexReferenceDocument.getFirst_page();
    String lastPageExtracted = openAlexReferenceDocument.getLast_page();

    if (firstPageExtracted == null || firstPageExtracted.strip().equals("")) {
      if (!(lastPageExtracted == null || lastPageExtracted.strip().equals(""))) {
        reference.setPages(lastPageExtracted);
      }
    } else {
      if (lastPageExtracted == null || lastPageExtracted.strip().equals("")) {
        reference.setPages(firstPageExtracted);
      } else {
        if (lastPageExtracted.strip().equals(firstPageExtracted)) {
          reference.setPages(firstPageExtracted);
        } else {
          reference.setPages(
              firstPageExtracted + "--" + lastPageExtracted, bibTexPagePreprocessing);
        }
      }
    }
    if (openAlexReferenceDocument.getPublication_type() != null) {

      String type = openAlexReferenceDocument.getPublication_type();
      if (type.equalsIgnoreCase("proceedings-article")
          || type.equalsIgnoreCase("book-chapter")
          || type.equalsIgnoreCase("conference")) {
        reference.setPublicationType("inproceedings");
      } else {
        if (type.equalsIgnoreCase("journal-article") || type.equalsIgnoreCase("journal")) {
          reference.setPublicationType("article");
        } else {
          if (type.equalsIgnoreCase("book")) {
            reference.setPublicationType("book");
          } else {
            if (type.equalsIgnoreCase("proceedings")) {
              reference.setPublicationType("proceedings");
            }
            reference.setPublicationType("misc");
          }
        }
      }
    }
    if (openAlexReferenceDocument.getPublisher() != null) {
      reference.setPublisher(openAlexReferenceDocument.getPublisher());
    }
    if (openAlexReferenceDocument.getTitle() != null) {
      reference.setTitleCorrect(openAlexReferenceDocument.getTitle());
    }
    if (openAlexReferenceDocument.getVolume() != null) {
      reference.setVolume(openAlexReferenceDocument.getVolume());
    }
    if (openAlexReferenceDocument.getYear() != null) {
      reference.setYear(openAlexReferenceDocument.getYear());
    }
    if (openAlexReferenceDocument.getIssn() != null) {
      reference.setISSN(openAlexReferenceDocument.getIssn());
    }
    if (openAlexReferenceDocument.getArxiv() != null) {
      reference.setLinkedArxivId(openAlexReferenceDocument.getArxiv());
    }
    reference.setCitationCount(openAlexReferenceDocument.getCitationCount());
    if (openAlexReferenceDocument.getPublicationDate() != null) {
      reference.setPublicationDate(openAlexReferenceDocument.getPublicationDate());
    }
    if (openAlexReferenceDocument.getArxiv() != null) {
      reference.setLinkedArxivId(openAlexReferenceDocument.getArxiv());
    }
    return reference;
  }
}
