package de.ippolis.wp3.local_database_literature_service.openalex.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Document(indexName = "openalexidx")
public class OpenAlexReferenceDocument {

  private String authors;
  private String licenses;
  @Id
  private String id;
  private String title;
  private String year;
  private String journal;
  private String volume;
  private String last_page;
  private String first_page;
  private String doi;
  private String publication_type;
  private String number;
  private String issn;
  private String publisher;
  private int citationCount;
  private boolean isOpenAccess;
  private boolean isRetracted;
  private String publicationDate;
  private String arxiv;

  public OpenAlexReferenceDocument(
      String authors,
      String licenses,
      String id,
      String title,
      String year,
      String journal,
      String volume,
      String last_page,
      String first_page,
      String doi,
      String publication_type,
      String number,
      String issn,
      String publisher,
      int citationCount,
      boolean isOpenAccess,
      boolean isRetracted,
      String publicationDate,
      String arxiv) {
    this.authors = authors;
    this.licenses = licenses;
    this.id = id;
    this.title = title;
    this.journal = journal;
    this.last_page = last_page;
    this.first_page = first_page;
    this.doi = doi;
    this.publication_type = publication_type;
    this.number = number;
    this.issn = issn;
    this.publisher = publisher;
    this.citationCount = citationCount;
    this.isOpenAccess = isOpenAccess;
    this.isRetracted = isRetracted;
    this.publicationDate = publicationDate;
    this.arxiv = arxiv;
    this.volume = volume;
    this.year = year;
  }

  public OpenAlexReferenceDocument() {
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume = volume;
  }

  public String getAuthors() {
    return authors;
  }

  public void setAuthors(String authors) {
    this.authors = authors;
  }

  public String getLicenses() {
    return licenses;
  }

  public void setLicenses(String licenses) {
    this.licenses = licenses;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getJournal() {
    return journal;
  }

  public void setJournal(String journal) {
    this.journal = journal;
  }

  public String getLast_page() {
    return last_page;
  }

  public void setLast_page(String last_page) {
    this.last_page = last_page;
  }

  public String getFirst_page() {
    return first_page;
  }

  public void setFirst_page(String first_page) {
    this.first_page = first_page;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getPublication_type() {
    return publication_type;
  }

  public void setPublication_type(String publication_type) {
    this.publication_type = publication_type;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getIssn() {
    return issn;
  }

  public void setIssn(String issn) {
    this.issn = issn;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public int getCitationCount() {
    return citationCount;
  }

  public void setCitationCount(int citationCount) {
    this.citationCount = citationCount;
  }

  public boolean isIsOpenAccess() {
    return isOpenAccess;
  }

  public void setIsOpenAccess(boolean isOpenAccess) {
    this.isOpenAccess = isOpenAccess;
  }

  public boolean isIsRetracted() {
    return isRetracted;
  }

  public void setIsRetracted(boolean isRetracted) {
    this.isRetracted = isRetracted;
  }

  public String getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(String publicationDate) {
    this.publicationDate = publicationDate;
  }

  public String getArxiv() {
    return arxiv;
  }

  public void setArxiv(String arxiv) {
    this.arxiv = arxiv;
  }
}
