package de.ippolis.wp3.local_database_literature_service.dblp.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Entity
@Document(indexName = "dblpidx")
public class DBLPReferenceDocument {

  private String authors;
  private String editors;
  @Id
  private String id;
  private String title;
  private String year;
  private String journal;
  private String volume;
  private String pages;
  private String doi;
  private String publication_type;
  private String issue;
  private String isbn;
  private String publisher;
  private String arxiv;

  public DBLPReferenceDocument() {
  }

  public DBLPReferenceDocument(
      String authors,
      String editors,
      String id,
      String title,
      String year,
      String journal,
      String volume,
      String pages,
      String doi,
      String publication_type,
      String issue,
      String isbn,
      String publisher,
      String arxiv) {
    this.authors = authors;
    this.editors = editors;
    this.id = id;
    this.title = title;
    this.year = year;
    this.journal = journal;
    this.volume = volume;
    this.pages = pages;
    this.doi = doi;
    this.publication_type = publication_type;
    this.issue = issue;
    this.isbn = isbn;
    this.publisher = publisher;
    this.arxiv = arxiv;
  }

  public String getAuthors() {
    return authors;
  }

  public void setAuthors(String authors) {
    this.authors = authors;
  }

  public String getEditors() {
    return editors;
  }

  public void setEditors(String editors) {
    this.editors = editors;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getJournal() {
    return journal;
  }

  public void setJournal(String journal) {
    this.journal = journal;
  }

  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume = volume;
  }

  public String getPages() {
    return pages;
  }

  public void setPages(String pages) {
    this.pages = pages;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getPublication_type() {
    return publication_type;
  }

  public void setPublication_type(String publication_type) {
    this.publication_type = publication_type;
  }

  public String getIssue() {
    return issue;
  }

  public void setIssue(String issue) {
    this.issue = issue;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public String getArxiv() {
    return arxiv;
  }

  public void setArxiv(String arxiv) {
    this.arxiv = arxiv;
  }
}
