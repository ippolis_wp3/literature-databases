package de.ippolis.wp3.literature_database_service.utils.model.json;

/**
 * JSON response representing a failure (e.g., invalid parameters)
 * <p>
 * Implementation of the JSend specification. See https://github.com/omniti-labs/jsend for details.
 */
public class FailJsonResponse extends DataJsonResponse {

  public FailJsonResponse(Object data) {
    super(JsonResponseStatus.FAIL, data);
  }
}
