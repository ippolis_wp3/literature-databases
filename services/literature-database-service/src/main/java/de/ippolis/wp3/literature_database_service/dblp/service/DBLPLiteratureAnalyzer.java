package de.ippolis.wp3.literature_database_service.dblp.service;

import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import de.ippolis.wp3.literature_database_service.utils.model.AbstractLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBLPLiteratureAnalyzer extends AbstractLiteratureAnalyzer {

  private final DBLPLocalDataBaseService dblpLocalDataBaseService;
  private final DBLPAPIReferenceService dblpapiReferenceService;

  @Autowired
  public DBLPLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      DBLPLocalDataBaseService dblpLocalDataBaseService,
      DBLPAPIReferenceService dblpapiReferenceService) {
    super(arxivHelper, bibTexHelperService, bibTexReferenceComparisonHelper);
    this.dblpLocalDataBaseService = dblpLocalDataBaseService;
    this.dblpapiReferenceService = dblpapiReferenceService;
  }

  @Override
  protected List<Reference> extractAPIResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing) {
    return new LinkedList<>();
  }

  @Override
  protected List<Reference> extractAPIResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    return dblpapiReferenceService.getDBLPReferencesByTitle(title, bibTexPagePreprocessing, id);
  }

  @Override
  protected List<Reference> extractAPIResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing) {
    return new LinkedList<>();
  }

  @Override
  protected List<Reference> extractLocalResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    return dblpLocalDataBaseService.getByTitle(title, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractLocalResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    return dblpLocalDataBaseService.getByDOI(doi, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractLocalResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (arxivId == null || arxivId.strip().equals("")) {
      return new LinkedList<>();
    }
    return dblpLocalDataBaseService.getByArxivId(arxivId, id, bibTexPagePreprocessing);
  }
}
