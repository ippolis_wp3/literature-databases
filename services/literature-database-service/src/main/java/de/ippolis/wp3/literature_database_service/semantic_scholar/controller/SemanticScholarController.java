package de.ippolis.wp3.literature_database_service.semantic_scholar.controller;

import de.ippolis.wp3.literature_database_service.semantic_scholar.service.SemanticScholarLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.dto.ArxivDoiTitleAuthorRequestDTO;
import de.ippolis.wp3.literature_database_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.SuccessJsonResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/** Example REST controller. */
@RestController
public class SemanticScholarController {
  private final SemanticScholarLiteratureAnalyzer semanticScholarLiteratureAnalyzer;
  Logger logger = LoggerFactory.getLogger(SemanticScholarController.class);

  @Autowired
  public SemanticScholarController(
      SemanticScholarLiteratureAnalyzer semanticScholarLiteratureAnalyzer) {

    this.semanticScholarLiteratureAnalyzer = semanticScholarLiteratureAnalyzer;
  }

  @PostMapping(
      value = "/semantic-scholar/getReferences",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchSemanticScholarReferences(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      List<Reference> result =
          this.semanticScholarLiteratureAnalyzer.resolveLiteratureEntry(
              id, doi, arxivId, title, authors, useLocalData, useAPI, bibTexPagePreprocessing);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/fetchNumberOfCitations",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchNumberOfCitations(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String doi = requestDTO.getDoi();
      String id = requestDTO.getId();
      String arxivId = requestDTO.getArxivId();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();

      int result =
          semanticScholarLiteratureAnalyzer.getNumberOfCitations(doi, arxivId, title, authors);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/fetchNumberOfHighlyInfluentialCitations",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchNumberOfHighlyInfluentialCitations(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String id = requestDTO.getId();
      String arxivId = requestDTO.getArxivId();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      int result =
          semanticScholarLiteratureAnalyzer.getNumberOfHighlyInfluentialCitations(
              id, arxivId, title, authors);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/semantic-scholar/resolveArxiv",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse resolveArxiv(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      List<String> authors = requestDTO.getAuthors();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> references =
          semanticScholarLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id, doi, arxivId, title, authors, useLocalData, useAPI, bibTexPagePreprocessing);
      String result = "";
      if (references.size() > 0) {
        result = references.get(0).toLatexStringBraces(true);
      }
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "semantic-scholar/resolveArxivFrontend",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse resolveArxivByArxivID(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {

    try {
      String arxivId = requestDTO.getArxivId();
      String doi = requestDTO.getDoi();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String id = requestDTO.getId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> results =
          semanticScholarLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id, doi, arxivId, title, authors, useLocalData, useAPI, bibTexPagePreprocessing);
      List<Map<String, Object>> resultsMap = new LinkedList<>();
      for (Reference reference : results) {
        resultsMap.add(reference.toSimpleMap(true));
      }
      logger.info("Response data: {}", resultsMap);
      SuccessJsonResponse response = new SuccessJsonResponse(resultsMap);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;

    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
