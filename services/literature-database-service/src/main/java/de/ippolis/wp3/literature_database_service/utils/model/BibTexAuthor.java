package de.ippolis.wp3.literature_database_service.utils.model;

import java.util.ArrayList;
import java.util.List;

public class BibTexAuthor {

  private String firstName;
  private String lastName;

  public BibTexAuthor(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public BibTexAuthor(String authorString) {
    List<TextCoordinate> stringParts = getStringParts(authorString);
    List<TextCoordinate> listNew = cleanUpCoordinateList(stringParts, authorString);

    int commaCounter = 0;
    int splitNum = 0;
    for (TextCoordinate coordinate : listNew) {
      String sub = authorString.substring(coordinate.getBeginIndex(), coordinate.getEndIndex());
      sub = sub.trim();
      if (sub.startsWith(",")) {
        commaCounter++;
        if (commaCounter == 1) {
          splitNum = coordinate.getBeginIndex();
        }
      } else {
        if (sub.endsWith(",")) {
          commaCounter++;
          if (commaCounter == 1) {
            splitNum = coordinate.getEndIndex();
          }
        }
      }
      if (commaCounter == 0) {
        splitNum = coordinate.getBeginIndex();
      }
    }
    if (commaCounter == 0) {

      String surname = authorString.substring(splitNum).trim();
      String firstname = authorString.substring(0, splitNum).trim();
      this.lastName = surname.trim();
      this.firstName = firstname.trim();
    } else {
      if (commaCounter == 1) {
        String surname = authorString.substring(0, splitNum).trim();
        if (surname.startsWith(",")) {
          surname = surname.substring(1).trim();
        }
        if (surname.endsWith(",")) {
          surname = surname.substring(0, surname.length() - 1).trim();
        }
        String firstname = authorString.substring(splitNum).trim();
        if (firstname.startsWith(",")) {
          firstname = firstname.substring(1).trim();
        }
        if (firstname.endsWith(",")) {
          firstname = firstname.substring(0, firstname.length() - 1).trim();
        }
        this.lastName = surname;
        this.firstName = firstname;
      }
    }
  }

  private List<TextCoordinate> getStringParts(String bibTexName) {
    ArrayList<TextCoordinate> list = new ArrayList<>();
    int counterBraces = 0;
    int beginPart = 0;

    int i;
    for (i = 0; i < bibTexName.length(); i++) {
      String character = bibTexName.substring(i, i + 1);
      if (character.equals("{")) {
        counterBraces++;
        if (beginPart != i && counterBraces == 1) {
          TextCoordinate coordinate = new TextCoordinate(beginPart, i + 1);
          list.add(coordinate);
          beginPart = i + 1;
        }
      } else {
        if (character.equals("}")) {

          counterBraces--;
        }
      }
      if (counterBraces == 0
          && (character.equals(" ") || character.equals(",") || character.equals("."))) {
        TextCoordinate coordinate = new TextCoordinate(beginPart, i + 1);
        list.add(coordinate);
        beginPart = i + 1;
      }
    }
    if (beginPart < i) {
      TextCoordinate coordinate = new TextCoordinate(beginPart, i);
      list.add(coordinate);
    }

    return list;
  }

  private List<TextCoordinate> cleanUpCoordinateList(
      List<TextCoordinate> stringParts, String name) {
    List<TextCoordinate> listNew = new ArrayList<>();

    int prevEnd = 0;
    for (TextCoordinate coord : stringParts) {
      String sub = name.substring(coord.getBeginIndex(), coord.getEndIndex());
      if (sub.startsWith("{")
          || sub.startsWith("}")
          || sub.startsWith(",")
          || sub.startsWith(" ")
          || sub.startsWith(".")) {
        sub = sub.substring(1);
      }
      if (sub.length() > 0) {
        TextCoordinate coordinate = new TextCoordinate(prevEnd, coord.getEndIndex());
        listNew.add(coordinate);
        prevEnd = coordinate.getEndIndex();
      }
    }
    return listNew;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String toBibTexAuthorString(boolean abbreviate) {
    String firstNamePreProc = firstName;
    if (abbreviate) {
      firstNamePreProc = preprocessFirstName(firstNamePreProc);
    }
    return lastName + ", " + firstNamePreProc + " ";
  }

  private String preprocessFirstName(String firstNamePreProc) {
    if (firstNamePreProc == null) {
      return "";
    }
    String preprocessed = "";
    String rest = firstNamePreProc.trim();
    rest = rest.replace(".", " ");
    rest = rest.replace(" -", "-");
    int indexMinus = rest.indexOf("-");
    int indexSpace = rest.indexOf(" ");
    while (indexMinus != -1 || indexSpace != -1) {
      if (indexSpace == -1) {
        String begin = rest.substring(0, indexMinus);
        if (begin.length() > 0) {
          preprocessed = preprocessed + begin.substring(0, 1) + ".-";
        }
        rest = rest.substring(indexMinus + 1);
      } else {
        if (indexMinus == -1) {
          String begin = rest.substring(0, indexSpace);
          if (begin.length() > 0) {
            preprocessed = preprocessed + begin.substring(0, 1) + ". ";
          }
          rest = rest.substring(indexSpace).trim();
        } else {
          if (indexMinus < indexSpace) {
            String begin = rest.substring(0, indexMinus).trim();
            if (begin.length() > 0) {
              preprocessed = preprocessed + begin.substring(0, 1) + ".-";
            }
            rest = rest.substring(indexMinus + 1).trim();
          } else {
            String begin = rest.substring(0, indexSpace).trim();
            if (begin.length() > 0) {
              preprocessed = preprocessed + begin.substring(0, 1) + ". ";
            }
            rest = rest.substring(indexSpace).trim();
          }
        }
      }
      indexMinus = rest.indexOf("-");
      indexSpace = rest.indexOf(" ");
    }
    if (rest.length() > 0) {
      if (preprocessed.equals("")) {
        preprocessed = rest.trim().substring(0, 1) + ". ";
      } else {
        preprocessed = preprocessed + rest.trim().substring(0, 1) + ". ";
      }
    }
    return preprocessed;
  }
}
