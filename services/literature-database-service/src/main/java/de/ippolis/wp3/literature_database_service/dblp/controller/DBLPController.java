package de.ippolis.wp3.literature_database_service.dblp.controller;

import de.ippolis.wp3.literature_database_service.dblp.service.DBLPLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.dto.ArxivDoiTitleAuthorRequestDTO;
import de.ippolis.wp3.literature_database_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.SuccessJsonResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DBLPController {
  private final DBLPLiteratureAnalyzer dblpLiteratureAnalyzer;
  private final ArxivHelper arxivHelper;
  private final Logger logger = LoggerFactory.getLogger(DBLPController.class);

  @Autowired
  public DBLPController(DBLPLiteratureAnalyzer dblpLiteratureAnalyzer, ArxivHelper arxivHelper) {
    this.dblpLiteratureAnalyzer = dblpLiteratureAnalyzer;
    this.arxivHelper = arxivHelper;
  }

  @PostMapping(
      value = "/dblp/getResultByDoiOrTitleAndAuthors",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getResultByDoiOrTitleAndAuthors(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    String title = requestDTO.getTitle();
    List<String> authors = requestDTO.getAuthors();
    String doi = requestDTO.getDoi();
    String id = requestDTO.getId();
    String arxivId = requestDTO.getArxivId();
    BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
    boolean useLocalData = requestDTO.isUseLocalData();
    boolean useAPIData = requestDTO.isUseAPI();
    List<Reference> results =
        dblpLiteratureAnalyzer.resolveLiteratureEntry(
            id, doi, arxivId, title, authors, useLocalData, useAPIData, bibTexPagePreprocessing);

    return new SuccessJsonResponse(results);
  }

  @PostMapping(
      value = "/dblp/resolveArxivToLatexString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse resolveArxivToLatexString(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    String title = requestDTO.getTitle();
    List<String> authors = requestDTO.getAuthors();
    String id = requestDTO.getId();
    String doi = requestDTO.getDoi();

    boolean useLocalData = requestDTO.isUseLocalData();
    boolean useAPIData = requestDTO.isUseAPI();
    String arxivId = requestDTO.getArxivId();
    BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
    List<Reference> results =
        dblpLiteratureAnalyzer.resolveLiteratureEntryArxiv(
            id, doi, arxivId, title, authors, useLocalData, useAPIData, bibTexPagePreprocessing);
    String latexString = "";
    if (results.size() > 0) {
      latexString = results.get(0).toLatexStringBraces(true);
    }
    return new SuccessJsonResponse(latexString);
  }

  @PostMapping(
      value = "/dblp/resolveArxivToMap",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivID(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String doi = requestDTO.getDoi();
      String id = requestDTO.getArxivId();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      String arxivId = requestDTO.getArxivId();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> references =
          dblpLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id, doi, arxivId, title, authors, useLocalData, useAPI, bibTexPagePreprocessing);
      List<Map<String, Object>> result = new LinkedList<>();
      for (Reference reference : references) {
        result.add(reference.toSimpleMap(true));
      }

      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
