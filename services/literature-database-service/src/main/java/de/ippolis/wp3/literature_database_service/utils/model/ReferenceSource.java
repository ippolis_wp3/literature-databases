package de.ippolis.wp3.literature_database_service.utils.model;

public enum ReferenceSource {
  SEMANTIC_SCHOLAR,
  CROSS_CITE,
  DBLP,
  OPENALEX,
}
