package de.ippolis.wp3.literature_database_service.openalex.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.ReferenceDeserializer;
import de.ippolis.wp3.literature_database_service.utils.service.APIRestService;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class OpenAlexAPIReferenceService {
  private final APIRestService apiRestService;

  public OpenAlexAPIReferenceService(APIRestService apiRestService) {
    this.apiRestService = apiRestService;
  }

  public List<Reference> getOpenAlexResultsByTitle(
      String title, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    if (title.strip().equals("")) {
      return new LinkedList<>();
    }

    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("title", title);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);

    final JSONObject response =
        this.apiRestService.performJsonPostRequest(
            "/open-alex/getAPIReferencesByTitle", requestParameters);
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        String referenceList = response.get("data").toString();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Reference.class, new ReferenceDeserializer());
        mapper.registerModule(module);
        try {

          Reference[] references = mapper.readValue(referenceList, Reference[].class);
          return List.of(references);
        } catch (JsonMappingException e) {
          e.printStackTrace();
        } catch (JsonProcessingException e) {
          e.printStackTrace();
        }
      }
    }
    return new LinkedList<>();
  }

  public List<Reference> getOpenAlexResultByDOI(
      String doi, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("doi", doi);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);

    final JSONObject response =
        this.apiRestService.performJsonPostRequest(
            "/open-alex/getAPIReferencesByDOI", requestParameters);
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        String referenceList = response.get("data").toString();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Reference.class, new ReferenceDeserializer());
        mapper.registerModule(module);
        try {

          Reference[] references = mapper.readValue(referenceList, Reference[].class);
          return List.of(references);
        } catch (JsonMappingException e) {
          e.printStackTrace();
        } catch (JsonProcessingException e) {
          e.printStackTrace();
        }
      }
    }
    return new LinkedList<>();
  }
}
