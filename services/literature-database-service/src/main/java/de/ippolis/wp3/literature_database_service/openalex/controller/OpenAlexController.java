package de.ippolis.wp3.literature_database_service.openalex.controller;

import de.ippolis.wp3.literature_database_service.openalex.service.OpenAlexLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.dto.ArxivDoiTitleAuthorRequestDTO;
import de.ippolis.wp3.literature_database_service.utils.model.json.ErrorJsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.JsonResponse;
import de.ippolis.wp3.literature_database_service.utils.model.json.SuccessJsonResponse;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OpenAlexController {
  private final OpenAlexLiteratureAnalyzer openAlexLiteratureAnalyzer;
  Logger logger = LoggerFactory.getLogger(OpenAlexController.class);

  @Autowired
  public OpenAlexController(OpenAlexLiteratureAnalyzer openAlexLiteratureAnalyzer) {

    this.openAlexLiteratureAnalyzer = openAlexLiteratureAnalyzer;
  }

  @PostMapping(
      value = "open-alex/getReferences",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getReferencesOpenAlex(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String id = requestDTO.getId();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      List<Reference> references =
          openAlexLiteratureAnalyzer.resolveLiteratureEntry(
              id, doi, arxivId, title, authors, useLocalData, useAPI, bibTexPagePreprocessing);
      logger.info("Response data: {}", references);
      SuccessJsonResponse response = new SuccessJsonResponse(references);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/open-alex/resolveArxivToString",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse getInfoOpenAlexByDoi(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      String id = requestDTO.getId();
      List<String> authors = requestDTO.getAuthors();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> references =
          openAlexLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              id, doi, arxivId, title, authors, useLocalData, useAPI, bibTexPagePreprocessing);
      String result = "";
      if (references.size() > 0) {
        result = references.get(0).toLatexStringBraces(true);
      }
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "/open-alex/fetchNumberOfCitations",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse fetchNumberOfCitations(
      @RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {
    try {
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      String doi = requestDTO.getDoi();
      String arxivId = requestDTO.getArxivId();
      int result =
          this.openAlexLiteratureAnalyzer.getNumberOfCitations(doi, arxivId, title, authors);
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;
    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }

  @PostMapping(
      value = "open-alex/resolveArxivFrontend",
      consumes = "application/json",
      produces = "application/json")
  public JsonResponse solveArxivByArxivID(@RequestBody ArxivDoiTitleAuthorRequestDTO requestDTO) {

    try {
      String arxivId = requestDTO.getArxivId();
      String doi = requestDTO.getDoi();
      String title = requestDTO.getTitle();
      List<String> authors = requestDTO.getAuthors();
      boolean useLocalData = requestDTO.isUseLocalData();
      boolean useAPI = requestDTO.isUseAPI();
      BibTexPagePreprocessing bibTexPagePreprocessing = requestDTO.getBibTexPagePreprocessing();
      List<Reference> references =
          openAlexLiteratureAnalyzer.resolveLiteratureEntryArxiv(
              arxivId, doi, arxivId, title, authors, useLocalData, useAPI, bibTexPagePreprocessing);
      List<Map<String, Object>> result = new LinkedList<>();
      for (Reference reference : references) {
        Map<String, Object> map = reference.toSimpleMap(true);
        result.add(map);
      }
      logger.info("Response data: {}", result);
      SuccessJsonResponse response = new SuccessJsonResponse(result);
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getData());
      return response;

    } catch (Exception e) {
      ErrorJsonResponse response = new ErrorJsonResponse(e.getMessage());
      logger.info("Response status and data: {}; {}", response.getStatus(), response.getMessage());
      return response;
    }
  }
}
