package de.ippolis.wp3.literature_database_service.utils.helper;

import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BibTexHelperService {
  private final Logger logger = LoggerFactory.getLogger(BibTexHelperService.class);

  public int extractNumberOfNonArxivReferences(
      List<Reference> infoDBLPByTitle, ArxivHelper arxivHelper) {
    int counter = 0;
    if (infoDBLPByTitle == null) {
      return 0;
    }
    if (infoDBLPByTitle.size() == 0) {
      return 0;
    }
    for (Reference ref : infoDBLPByTitle) {
      if (!ref.isArxiv(arxivHelper)) {
        counter++;
      }
    }
    return counter;
  }

  public List<Reference> removeArxivReferencesFromList(
      List<Reference> infoDBLPByTitle, ArxivHelper arxivHelper) {
    ArrayList<Reference> reducedList = new ArrayList<>();
    for (Reference reference : infoDBLPByTitle) {
      if (!reference.isArxiv(arxivHelper)) {
        reducedList.add(reference);
      }
    }
    return reducedList;
  }

  public List<Reference> extractArxivReferencesFromList(
      List<Reference> references, ArxivHelper arxivHelper) {
    ArrayList<Reference> reducedList = new ArrayList<>();
    for (Reference reference : references) {
      if (reference.isArxiv(arxivHelper)) {
        reducedList.add(reference);
      }
    }
    return reducedList;
  }
}
