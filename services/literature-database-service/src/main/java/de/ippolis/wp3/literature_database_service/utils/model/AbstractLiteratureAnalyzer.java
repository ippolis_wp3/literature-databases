package de.ippolis.wp3.literature_database_service.utils.model;

import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractLiteratureAnalyzer {
  private final ArxivHelper arxivHelper;
  private final BibTexHelperService bibTexHelperService;
  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;

  @Autowired
  public AbstractLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper) {
    this.arxivHelper = arxivHelper;
    this.bibTexHelperService = bibTexHelperService;
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;
  }

  public List<Reference> resolveLiteratureEntry(
      String id,
      String doi,
      String arxivId,
      String title,
      List<String> authors,
      boolean useLocalDatabase,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    List<Reference> arxivResultsLocalArxivId = new LinkedList<>();
    List<Reference> arxivResultsLocalDoi = new LinkedList<>();
    List<Reference> arxivResultsLocalTitle = new LinkedList<>();

    List<Reference> arxivResultsAPIArxivId = new LinkedList<>();
    List<Reference> arxivResultsAPIDoi = new LinkedList<>();
    List<Reference> arxivResultsAPITitle = new LinkedList<>();
    if (useLocalDatabase) {
      List<Reference> localResultsByArxivId =
          extractLocalResultsByArxivId(id, arxivId, bibTexPagePreprocessing);
      List<Reference> filteredListArxivIdLocal = arxivIdFilter(localResultsByArxivId, arxivId);
      filteredListArxivIdLocal = generalFilter(filteredListArxivIdLocal);
      arxivResultsLocalArxivId = identifyDirectArxivReferences(filteredListArxivIdLocal);
      filteredListArxivIdLocal.removeAll(arxivResultsLocalArxivId);
      if (filteredListArxivIdLocal.size() > 0) {
        return filteredListArxivIdLocal;
      }
      List<Reference> localResultsByDOI =
          extractLocalResultsByDOI(id, doi, bibTexPagePreprocessing);
      List<Reference> filteredListDOILocal = doiFilter(localResultsByDOI, doi);
      arxivResultsLocalDoi = identifyDirectArxivReferences(filteredListDOILocal);
      filteredListDOILocal.removeAll(arxivResultsLocalDoi);
      if (filteredListDOILocal.size() > 0) {
        return filteredListDOILocal;
      }
      List<Reference> localResultsByTitle =
          extractLocalResultsByTitle(id, title, bibTexPagePreprocessing);
      List<Reference> filteredListTitleLocal = titleFilter(localResultsByTitle, title, authors);
      arxivResultsLocalTitle = identifyDirectArxivReferences(filteredListTitleLocal);
      filteredListTitleLocal.removeAll(arxivResultsLocalTitle);
      if (filteredListTitleLocal.size() > 0) {
        return filteredListTitleLocal;
      }
    }

    if (useAPI) {
      List<Reference> localResultsByArxivId =
          extractAPIResultsByArxivId(id, arxivId, bibTexPagePreprocessing);
      List<Reference> filteredListArxivIdAPI = arxivIdFilter(localResultsByArxivId, arxivId);
      arxivResultsAPIArxivId = identifyDirectArxivReferences(filteredListArxivIdAPI);
      filteredListArxivIdAPI.removeAll(arxivResultsAPIArxivId);
      if (filteredListArxivIdAPI.size() > 0) {
        return filteredListArxivIdAPI;
      }
      List<Reference> localResultsByDOI = extractAPIResultsByDOI(id, doi, bibTexPagePreprocessing);
      List<Reference> filteredListDoiAPI = doiFilter(localResultsByDOI, doi);
      arxivResultsAPIDoi = identifyDirectArxivReferences(filteredListDoiAPI);
      filteredListDoiAPI.removeAll(arxivResultsAPIDoi);
      if (filteredListDoiAPI.size() > 0) {
        return filteredListDoiAPI;
      }
      List<Reference> localResultsByTitle =
          extractAPIResultsByTitle(id, title, bibTexPagePreprocessing);
      List<Reference> filteredListTitleAPI = titleFilter(localResultsByTitle, title, authors);
      arxivResultsAPITitle = identifyDirectArxivReferences(filteredListTitleAPI);
      filteredListTitleAPI.removeAll(arxivResultsAPITitle);
      if (filteredListTitleAPI.size() > 0) {
        return filteredListTitleAPI;
      }
    }
    if (arxivResultsLocalArxivId.size() > 0) {
      return arxivResultsLocalArxivId;
    }
    if (arxivResultsLocalDoi.size() > 0) {
      return arxivResultsLocalDoi;
    }
    if (arxivResultsLocalTitle.size() > 0) {
      return arxivResultsLocalTitle;
    }
    if (arxivResultsAPIArxivId.size() > 0) {
      return arxivResultsAPIArxivId;
    }
    if (arxivResultsAPIDoi.size() > 0) {
      return arxivResultsAPIDoi;
    }
    if (arxivResultsAPITitle.size() > 0) {
      return arxivResultsAPITitle;
    }
    return new LinkedList<>();
  }

  public List<Reference> resolveLiteratureEntryArxiv(
      String id,
      String doi,
      String arxivId,
      String title,
      List<String> authors,
      boolean useLocalDatabase,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    List<Reference> references =
        resolveLiteratureEntryArxivFiltered(
            id, doi, arxivId, title, authors, useLocalDatabase, useAPI, bibTexPagePreprocessing);
    if (references.size() > 1) {
      List<Reference> filtered = identifyBestMatches(references, title, authors);
      return filtered;
    }
    return references;
  }

  private double calculateMinLevenshteinRatio(List<Reference> references, String title) {
    double min = 2.0;
    for (Reference reference : references) {
      double distanceRatio =
          bibTexReferenceComparisonHelper.calculateLevenshteinDistanceRatio(
              title, reference.getTitleCorrect());
      if (distanceRatio < min) {
        min = distanceRatio;
      }
    }
    return min;
  }

  private List<Reference> identifyBestMatches(
      List<Reference> references, String title, List<String> authors) {
    double minLevenshtein = calculateMinLevenshteinRatio(references, title);
    List<Reference> filteredByLevenshtein =
        minLevenshteinDistanceFilter(references, title, minLevenshtein);
    if (filteredByLevenshtein.size() > 1) {
      double maxAuthorsMatchingRatio =
          calculateMaxAuthorsMatchingRatio(filteredByLevenshtein, authors);
      List<Reference> filteredByAuthors =
          maxAuthorsMatchingFilter(filteredByLevenshtein, authors, maxAuthorsMatchingRatio);
      return filteredByAuthors;
    } else {
      return filteredByLevenshtein;
    }
  }

  private List<Reference> maxAuthorsMatchingFilter(
      List<Reference> references, List<String> authors, double maxAuthorsMatchingRatio) {
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      double authorRatioCalculated =
          bibTexReferenceComparisonHelper.calculateAuthorRatio(reference.getSurnames(), authors);
      if (authorRatioCalculated == maxAuthorsMatchingRatio) {
        filteredList.add(reference);
      }
    }
    return filteredList;
  }

  private double calculateMaxAuthorsMatchingRatio(
      List<Reference> references, List<String> authors) {
    double max = -1.0;
    for (Reference reference : references) {
      double matchingRatio =
          bibTexReferenceComparisonHelper.calculateAuthorRatio(reference.getSurnames(), authors);
      if (matchingRatio > max) {
        max = matchingRatio;
      }
    }
    return max;
  }

  private List<Reference> minLevenshteinDistanceFilter(
      List<Reference> references, String title, double minLevenshtein) {
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      double levenshteinCalculated =
          bibTexReferenceComparisonHelper.calculateLevenshteinDistanceRatio(
              reference.getTitleCorrect(), title);
      if (levenshteinCalculated == minLevenshtein) {
        filteredList.add(reference);
      }
    }
    return filteredList;
  }

  private List<Reference> resolveLiteratureEntryArxivFiltered(
      String id,
      String doi,
      String arxivId,
      String title,
      List<String> authors,
      boolean useLocalDatabase,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (useLocalDatabase) {
      List<Reference> localResultsByArxivId =
          extractLocalResultsByArxivId(id, arxivId, bibTexPagePreprocessing);
      List<Reference> filteredListArxivIdLocal = arxivIdFilter(localResultsByArxivId, arxivId);
      filteredListArxivIdLocal = generalFilter(filteredListArxivIdLocal);
      filteredListArxivIdLocal = filtersForPreprintResolver(filteredListArxivIdLocal);
      List<Reference> arxivResultsLocalArxivId =
          identifyDirectArxivReferences(filteredListArxivIdLocal);
      filteredListArxivIdLocal.removeAll(arxivResultsLocalArxivId);
      if (filteredListArxivIdLocal.size() > 0) {
        return filteredListArxivIdLocal;
      }
      List<Reference> localResultsByDOI =
          extractLocalResultsByDOI(id, doi, bibTexPagePreprocessing);
      List<Reference> filteredListDOILocal = doiFilter(localResultsByDOI, doi);
      filteredListDOILocal = filtersForPreprintResolver(filteredListDOILocal);
      List<Reference> arxivResultsLocalDoi = identifyDirectArxivReferences(filteredListDOILocal);
      filteredListDOILocal.removeAll(arxivResultsLocalDoi);
      if (filteredListDOILocal.size() > 0) {
        return filteredListDOILocal;
      }
      List<Reference> localResultsByTitle =
          extractLocalResultsByTitle(id, title, bibTexPagePreprocessing);
      List<Reference> filteredListTitleLocal = titleFilter(localResultsByTitle, title, authors);
      filteredListTitleLocal = filtersForPreprintResolver(filteredListTitleLocal);
      List<Reference> arxivResultsLocalTitle =
          identifyDirectArxivReferences(filteredListTitleLocal);
      filteredListTitleLocal.removeAll(arxivResultsLocalTitle);
      if (filteredListTitleLocal.size() > 0) {
        return filteredListTitleLocal;
      }
    }
    if (useAPI) {
      List<Reference> localResultsByArxivId =
          extractAPIResultsByArxivId(id, arxivId, bibTexPagePreprocessing);
      List<Reference> filteredListArxivIdAPI = arxivIdFilter(localResultsByArxivId, arxivId);
      filteredListArxivIdAPI = filtersForPreprintResolver(filteredListArxivIdAPI);
      List<Reference> arxivResultsAPIArxivId =
          identifyDirectArxivReferences(filteredListArxivIdAPI);
      filteredListArxivIdAPI.removeAll(arxivResultsAPIArxivId);
      if (filteredListArxivIdAPI.size() > 0) {
        return filteredListArxivIdAPI;
      }
      List<Reference> localResultsByDOI = extractAPIResultsByDOI(id, doi, bibTexPagePreprocessing);
      List<Reference> filteredListDoiAPI = doiFilter(localResultsByDOI, doi);
      filteredListDoiAPI = filtersForPreprintResolver(filteredListDoiAPI);
      List<Reference> arxivResultsAPIDoi = identifyDirectArxivReferences(filteredListDoiAPI);
      filteredListDoiAPI.removeAll(arxivResultsAPIDoi);
      if (filteredListDoiAPI.size() > 0) {
        return filteredListDoiAPI;
      }
      List<Reference> localResultsByTitle =
          extractAPIResultsByTitle(id, title, bibTexPagePreprocessing);
      List<Reference> filteredListTitleAPI = titleFilter(localResultsByTitle, title, authors);
      filteredListTitleAPI = filtersForPreprintResolver(filteredListTitleAPI);
      List<Reference> arxivResultsAPITitle = identifyDirectArxivReferences(filteredListTitleAPI);
      filteredListTitleAPI.removeAll(arxivResultsAPITitle);
      if (filteredListTitleAPI.size() > 0) {
        return filteredListTitleAPI;
      }
    }
    return new LinkedList<>();
  }

  private List<Reference> filtersForPreprintResolver(List<Reference> references) {
    List<Reference> results = new LinkedList<>();
    for (Reference reference : references) {
      if (!reference.getJournal().strip().equals("")) {
        if (!(reference.getPublicationType().strip().equals("")
            || reference.getPublicationType().equalsIgnoreCase("misc"))) {
          results.add(reference);
        }
      }
    }
    return results;
  }

  public int getNumberOfCitations(String doi, String arxivId, String title, List<String> authors) {
    List<Reference> references =
        this.resolveLiteratureEntry(
            "",
            doi,
            arxivId,
            title,
            authors,
            false,
            true,
            BibTexPagePreprocessing.NO_PREPROCESSING);
    if (references.size() > 0) {
      return references.get(0).getCitationCount();
    }
    return -1;
  }

  public int getNumberOfHighlyInfluentialCitations(
      String doi, String arxivId, String title, List<String> authors) {
    List<Reference> references =
        this.resolveLiteratureEntry(
            "",
            doi,
            arxivId,
            title,
            authors,
            false,
            true,
            BibTexPagePreprocessing.NO_PREPROCESSING);
    if (references.size() > 0) {
      return references.get(0).getHighlyInfluentialCitationCount();
    }
    return -1;
  }

  private List<Reference> generalFilter(List<Reference> references) {
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      if (reference.getRetracted() == null || reference.getRetracted() == false) {
        filteredList.add(reference);
      }
    }
    return filteredList;
  }

  protected abstract List<Reference> extractAPIResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing);

  protected abstract List<Reference> extractAPIResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing);

  protected abstract List<Reference> extractAPIResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing);

  protected abstract List<Reference> extractLocalResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing);

  protected abstract List<Reference> extractLocalResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing);

  protected abstract List<Reference> extractLocalResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing);

  private List<Reference> doiFilter(List<Reference> references, String doi) {
    if (doi == null || bibTexReferenceComparisonHelper.preprocessDOI(doi).equals("")) {
      return new LinkedList<>();
    }
    List<Reference> filteredList = new LinkedList<>();
    for (Reference reference : references) {
      if (bibTexReferenceComparisonHelper
          .preprocessDOI(reference.getDoi())
          .equals(bibTexReferenceComparisonHelper.preprocessDOI(doi))) {
        filteredList.add(reference);
      }
    }
    return filteredList;
  }

  private List<Reference> arxivIdFilter(List<Reference> references, String arxivId) {
    List<Reference> filteredList = new LinkedList<>();
    if (arxivId == null || arxivHelper.preprocessArxiv(arxivId).equals("")) {
      return new LinkedList<>();
    }
    for (Reference reference : references) {
      if (arxivHelper
          .preprocessArxiv(arxivId)
          .equals(arxivHelper.preprocessArxiv(reference.getLinkedArxivId()))) {
        filteredList.add(reference);
      } else {
        if (arxivHelper.preprocessArxiv(reference.getLinkedArxivId()).equals("")) {
          filteredList.add(reference);
        }
      }
    }
    return filteredList;
  }

  private List<Reference> titleFilter(
      List<Reference> references, String title, List<String> authors) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    return bibTexReferenceComparisonHelper.filterMatches(references, authors, title);
  }

  private List<Reference> identifyDirectArxivReferences(List<Reference> localResultsByArxivId) {
    List<Reference> filteredList =
        bibTexHelperService.extractArxivReferencesFromList(localResultsByArxivId, arxivHelper);
    return filteredList;
  }
}
