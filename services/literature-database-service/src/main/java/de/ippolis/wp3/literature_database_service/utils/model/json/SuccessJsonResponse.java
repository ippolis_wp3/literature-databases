package de.ippolis.wp3.literature_database_service.utils.model.json;

/**
 * JSON response representing a successful result
 * <p>
 * Implementation of the JSend specification. See
 * <a href="https://github.com/omniti-labs/jsend">omniti-labs/jsend</a> for details.
 */
public class SuccessJsonResponse extends DataJsonResponse {

  public SuccessJsonResponse() {
    this(JsonResponseStatus.SUCCESS);
  }

  public SuccessJsonResponse(Object data) {
    super(JsonResponseStatus.SUCCESS, data);
  }
}
