package de.ippolis.wp3.literature_database_service.utils.model.dto;

import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;

public class RequestDOIDto {
  private String doi;
  private String id;
  private boolean useLocalData;
  private boolean useAPI;
  private BibTexPagePreprocessing bibTexPagePreprocessing;

  public RequestDOIDto() {}

  public RequestDOIDto(
      String doi,
      String id,
      boolean useLocalData,
      boolean useAPI,
      BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.doi = doi;
    this.id = id;
    this.useLocalData = useLocalData;
    this.useAPI = useAPI;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public boolean isUseLocalData() {
    return useLocalData;
  }

  public void setUseLocalData(boolean useLocalData) {
    this.useLocalData = useLocalData;
  }

  public boolean isUseAPI() {
    return useAPI;
  }

  public void setUseAPI(boolean useAPI) {
    this.useAPI = useAPI;
  }
}
