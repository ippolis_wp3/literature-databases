package de.ippolis.wp3.literature_database_service.utils.model;

import java.util.LinkedList;import java.util.List;import java.util.regex.Matcher;import java.util.regex.Pattern;import org.springframework.stereotype.Service;

@Service
public class TextHelperService {
  /**
   * extracts list of indexes of a given pattern in a given text
   *
   * @param text given text
   * @param patternStr given pattern
   * @return list of text ranges found
   */
  public List<TextRange> getIndexesOfPatternInText(String text, String patternStr) {
    List<TextRange> indexes = new LinkedList<>();
    if (text == null || patternStr == null) {
      return indexes;
    }

    Pattern pattern = Pattern.compile(patternStr);
    Matcher matcher = pattern.matcher(text);
    while (matcher.find()) {
      int start = matcher.start();
      int end = matcher.end();
      TextRange textRange = new TextRange(start, end);
      indexes.add(textRange);
    }
    return (indexes);
  }

  public boolean checkArxivInText(String text) {
    if (text.toLowerCase().contains("corr")) {
      return true;
    }
    if (text.toLowerCase().contains("arxiv")) {
      return true;
    }
    return text.toLowerCase().contains("computing research repository");
  }
}
