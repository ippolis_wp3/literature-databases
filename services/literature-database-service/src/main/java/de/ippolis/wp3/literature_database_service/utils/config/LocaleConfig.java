package de.ippolis.wp3.literature_database_service.utils.config;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Set up locale configuration.
 */
@Configuration
public class LocaleConfig implements WebMvcConfigurer {

  Logger logger = LoggerFactory.getLogger(LocaleConfig.class);

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(localeChangeInterceptor());
    logger.debug("Locale change interceptor created and added to registry");
  }

  /**
   * Add the default locale change interceptor.
   *
   * It will extract and change the locale based on the "locale" request parameter.
   */
  @Bean
  public LocaleChangeInterceptor localeChangeInterceptor() {
    return new LocaleChangeInterceptor();
  }

  /**
   * Add a locale resolver which allows us to change the locale.
   */
  @Bean
  public LocaleResolver localeResolver() {
    SessionLocaleResolver slr = new SessionLocaleResolver();
    slr.setDefaultLocale(Locale.US);

    logger.debug("LocaleResolver created");

    return slr;
  }

  /**
   * Set up the message source.
   *
   * Our locales are searched for in locale/messages_LOCALE.properties on the classpath.
   */
  @Bean
  public MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("locale/messages");
    messageSource.setDefaultEncoding("UTF-8");

    logger.debug("Message source created");

    return messageSource;
  }

}
