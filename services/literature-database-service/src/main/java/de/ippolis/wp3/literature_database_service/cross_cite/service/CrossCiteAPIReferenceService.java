package de.ippolis.wp3.literature_database_service.cross_cite.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.ReferenceDeserializer;
import de.ippolis.wp3.literature_database_service.utils.service.APIRestService;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrossCiteAPIReferenceService {
  private final BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper;
  private final APIRestService apiRestService;

  @Autowired
  public CrossCiteAPIReferenceService(
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      APIRestService apiRestService) {
    this.bibTexReferenceComparisonHelper = bibTexReferenceComparisonHelper;

    this.apiRestService = apiRestService;
  }

  public List<Reference> getInfoCrossCiteByDOI(
      String doi, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("id", id);
    requestParameters.put("doi", doi);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);

    final JSONObject response =
        this.apiRestService.performJsonPostRequest(
            "/cross-cite/getAPIReferencesByDOI", requestParameters);
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        String referenceList = response.get("data").toString();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Reference.class, new ReferenceDeserializer());
        mapper.registerModule(module);
        try {

          Reference[] references = mapper.readValue(referenceList, Reference[].class);
          return List.of(references);
        } catch (JsonMappingException e) {
          e.printStackTrace();
        } catch (JsonProcessingException e) {
          e.printStackTrace();
        }
      }
    }
    return new LinkedList<>();
  }

  public List<Reference> getInfoCrossRefByTitle(
      String title, BibTexPagePreprocessing bibTexPagePreprocessing, String id) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    JSONObject requestParameters = new JSONObject();
    requestParameters.put("title", title);
    requestParameters.put("id", id);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);

    final JSONObject response =
        this.apiRestService.performJsonPostRequest(
            "/cross-cite/getAPIReferencesByTitle", requestParameters);
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        String referenceList = response.get("data").toString();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Reference.class, new ReferenceDeserializer());
        mapper.registerModule(module);
        try {

          Reference[] references = mapper.readValue(referenceList, Reference[].class);
          return List.of(references);
        } catch (JsonMappingException e) {
          e.printStackTrace();
        } catch (JsonProcessingException e) {
          e.printStackTrace();
        }
      }
    }
    return new LinkedList<>();
  }
}
