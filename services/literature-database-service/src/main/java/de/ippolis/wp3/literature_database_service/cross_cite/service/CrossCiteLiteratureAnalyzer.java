package de.ippolis.wp3.literature_database_service.cross_cite.service;

import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import de.ippolis.wp3.literature_database_service.utils.model.AbstractLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrossCiteLiteratureAnalyzer extends AbstractLiteratureAnalyzer {
  private final CrossCiteLocalDatabaseService crossCiteLocalDatabaseService;
  private final CrossCiteAPIReferenceService crossCiteAPIReferenceService;

  @Autowired
  public CrossCiteLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      CrossCiteLocalDatabaseService crossCiteLocalDatabaseService,
      CrossCiteAPIReferenceService crossCiteAPIReferenceService) {
    super(arxivHelper, bibTexHelperService, bibTexReferenceComparisonHelper);
    this.crossCiteLocalDatabaseService = crossCiteLocalDatabaseService;
    this.crossCiteAPIReferenceService = crossCiteAPIReferenceService;
  }

  @Override
  protected List<Reference> extractAPIResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null) {
      return new LinkedList<>();
    }
    return crossCiteAPIReferenceService.getInfoCrossCiteByDOI(doi, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractAPIResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();

    } else {
      return crossCiteAPIReferenceService.getInfoCrossRefByTitle(
          title, bibTexPagePreprocessing, id);
    }
  }

  @Override
  protected List<Reference> extractAPIResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing) {
    return new LinkedList<>();
  }

  @Override
  protected List<Reference> extractLocalResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    return crossCiteLocalDatabaseService.getByTitle(title, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractLocalResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    return crossCiteLocalDatabaseService.getByDOI(doi, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractLocalResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing) {
    return new LinkedList<>();
  }
}
