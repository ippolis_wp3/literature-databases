package de.ippolis.wp3.literature_database_service.utils.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class Reference {
  private final ArrayList<BibTexAuthor> authors = new ArrayList<>();
  private final ArrayList<BibTexAuthor> editor = new ArrayList<>();
  private final List<String> licenses = new ArrayList<>();
  private String id;
  private String titleCorrect = "";
  private String year = "";
  private String journal = "";
  private String volume = "";
  private String pages = "";
  private String doi = "";
  private String publicationType = "";
  private String issue = "";
  private String ISSN = "";
  private String ISBN = "";
  private String publisher = "";
  private String publisherLocation = "";
  private int citationCount = -1;
  private int highlyInfluentialCitationCount = -1;
  private String abstractText = "";
  private Boolean isOpenAccess = null;
  private Boolean isRetracted = null;
  private String publicationDate = "";
  private String linkedArxivId = "";

  public Reference(String id) {
    this.id = id;
  }

  public Reference(
      Map<String, Object> match,
      String id,
      ReferenceSource source,
      BibTexPagePreprocessing bibTexPageProcessing) {
    BibTexPagePreprocessing bibTexPagePreprocessingNeeded;
    if (bibTexPageProcessing == BibTexPagePreprocessing.NO_PREPROCESSING) {
      bibTexPagePreprocessingNeeded = BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES;
    } else {
      bibTexPagePreprocessingNeeded = bibTexPageProcessing;
    }
    if (source == ReferenceSource.SEMANTIC_SCHOLAR) {
      this.processSemanticScholarData(match, id, bibTexPagePreprocessingNeeded);
    } else {
      if (source == ReferenceSource.DBLP) {
        this.processDBLP(match, id, bibTexPagePreprocessingNeeded);
      } else {
        if (source == ReferenceSource.CROSS_CITE) {
          this.processCrossCiteObject(match, id, bibTexPagePreprocessingNeeded);
        } else {
          if (source == ReferenceSource.OPENALEX) {
            this.processOpenAlexObject(match, id, bibTexPagePreprocessingNeeded);
          }
        }
      }
    }
  }

  public void addLicense(String license) {
    if (!licenses.contains(license.toLowerCase())) {
      licenses.add(license.toLowerCase());
    }
  }

  public Boolean getOpenAccess() {
    return isOpenAccess;
  }

  public void setOpenAccess(Boolean openAccess) {
    isOpenAccess = openAccess;
  }

  public Boolean getRetracted() {
    return isRetracted;
  }

  public void setRetracted(Boolean retracted) {
    isRetracted = retracted;
  }

  public String getLinkedArxivId() {
    return linkedArxivId;
  }

  public void setLinkedArxivId(String linkedArxivId) {
    this.linkedArxivId = linkedArxivId;
  }

  public List<String> getLicenses() {
    return this.licenses;
  }

  public String getPublicationDate() {
    return publicationDate;
  }

  public void setPublicationDate(String publicationDate) {
    this.publicationDate = publicationDate;
  }

  private void processOpenAlexObject(
      Map<String, Object> match, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (id != null) {
      this.setId(id);
    }
    if (match.containsKey("type")) {
      Object typeObject = match.get("type");
      if (typeObject instanceof String) {
        String type = match.get("type").toString();
        if (type.equalsIgnoreCase("journal") || type.equalsIgnoreCase("journal-article")) {
          this.setPublicationType("article");
        } else {
          if (type.equalsIgnoreCase("conference")
              || type.equalsIgnoreCase("proceedings-article")
              || type.equalsIgnoreCase("book-chapter")) {
            this.setPublicationType("inproceedings");
          } else {
            if (type.equalsIgnoreCase("proceedings")) {
              this.setPublicationType("proceedings");
            } else {
              if (type.equals("book")) {
                this.setPublicationType("book");
              }
            }
          }
        }
      }
    }
    if (!this.publicationType.equalsIgnoreCase("proceedings")) {
      if (match.containsKey("authorships")) {
        if (match.get("authorships") instanceof ArrayList<?>) {
          ArrayList<Object> objectResultsAuthorList = (ArrayList<Object>) match.get("authorships");
          for (Object authorObject : objectResultsAuthorList) {
            if (authorObject instanceof Map<?, ?>) {
              Map<String, Object> authorMap = ((Map<String, Object>) authorObject);
              if (authorMap.containsKey("author")) {
                Object authorObj = authorMap.get("author");
                if (authorObj instanceof Map<?, ?>) {
                  Map<String, Object> authorM = (Map<String, Object>) authorObj;
                  if (authorM.containsKey("display_name")) {
                    Object authorNameObject = authorM.get("display_name");
                    if (authorNameObject instanceof String) {
                      String authorName = (String) authorNameObject;
                      addAuthor(authorName);
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      if (match.containsKey("authorships")) {
        if (match.get("authorships") instanceof ArrayList<?>) {
          ArrayList<Object> objectResultsAuthorList = (ArrayList<Object>) match.get("authorships");
          for (Object authorObject : objectResultsAuthorList) {
            if (authorObject instanceof Map<?, ?>) {
              Map<String, Object> authorMap = ((Map<String, Object>) authorObject);
              if (authorMap.containsKey("author")) {
                Object authorObj = authorMap.get("author");
                if (authorObj instanceof Map<?, ?>) {
                  Map<String, Object> authorM = (Map<String, Object>) authorObj;
                  if (authorM.containsKey("display_name")) {
                    Object authorNameObject = authorM.get("display_name");
                    if (authorNameObject instanceof String) {
                      String authorName = (String) authorNameObject;
                      addEditor(authorName);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if (match.containsKey("host_venue")) {
      Object venue = match.get("host_venue");
      if (venue instanceof Map<?, ?>) {
        Map<String, Object> venueMap = (Map<String, Object>) venue;
        if (venueMap.containsKey("display_name")) {
          Object journalObject = venueMap.get("display_name");
          if (journalObject instanceof String) {
            String journalName = journalObject.toString();
            this.setJournal(journalName);
          }
        }

        if (venueMap.containsKey("publisher")) {
          Object publisherObject = venueMap.get("publisher");
          if (publisherObject instanceof String) {
            String publisher = publisherObject.toString();
            this.setPublisher(publisher);
          }
        }
      }
    }
    if (getJournal() == null || getJournal().equals("")) {
      if (match.containsKey("primary_location")) {
        Object location = match.get("primary_location");
        if (location != null) {
          Map<String, Object> locationMap = (Map<String, Object>) location;
          if (locationMap.containsKey("source")) {
            Object locationSource = locationMap.get("source");
            if (locationSource != null) {
              Map<String, Object> locationSourceMap = (Map<String, Object>) locationSource;
              if (locationSourceMap.containsKey("display_name")) {
                Object locationSourceDisplayName = locationSourceMap.get("display_name");
                if (locationSourceDisplayName instanceof String) {
                  String locationSourceDisplayNameString = locationSourceDisplayName.toString();
                  setJournal(locationSourceDisplayNameString);
                }
              }
            }
          }
        }
      }
    }
    if (match.containsKey("title")) {
      Object titleObject = match.get("title");
      if (titleObject instanceof String) {
        String title = titleObject.toString();
        String titlePreProcessed = preProcessTitle(title);
        titlePreProcessed = preProcessTitleDatabase(titlePreProcessed);
        this.setTitleCorrect(titlePreProcessed);
      }
    }
    if (match.containsKey("biblio")) {
      Object biblio = match.get("biblio");
      if (biblio instanceof Map<?, ?>) {
        Map<String, Object> biblioMap = (Map<String, Object>) biblio;
        if (biblioMap.containsKey("volume")) {
          Object volumeObject = biblioMap.get("volume");
          if (volumeObject instanceof String) {
            String volume = volumeObject.toString();
            this.setVolume(volume);
          }
        }
        String firstPage = "";
        String lastPage = "";

        if (biblioMap.containsKey("first_page")) {
          Object firstPageObject = biblioMap.get("first_page");
          if (firstPageObject instanceof String) {
            firstPage = firstPageObject.toString();
          }
        }
        if (biblioMap.containsKey("last_page")) {
          Object lastPageObject = biblioMap.get("last_page");
          if (lastPageObject instanceof String) {
            lastPage = lastPageObject.toString();
          }
        }
        if (!(firstPage.equals("") && lastPage.equals(""))) {
          if (!(firstPage.equals("") || lastPage.equals(""))) {
            this.setPages(firstPage + "-" + lastPage, bibTexPagePreprocessing);
          }
        }
        if (biblioMap.containsKey("issue")) {
          Object issueObject = biblioMap.get("issue");
          if (issueObject instanceof String) {
            String issue = issueObject.toString();
            this.setIssue(issue);
          }
        }
      }
    }
    if (match.containsKey("publication_year")) {
      Object publicationYearObject = match.get("publication_year");
      if (publicationYearObject instanceof Integer) {
        int publicationYear = (int) publicationYearObject;
        String publicationYearString = String.valueOf(publicationYear);
        this.setYear(publicationYearString);
      } else {
        if (publicationYearObject instanceof String) {
          String publicationYearString = publicationYearObject.toString();
          this.setYear(publicationYearString);
        }
      }
    }

    if (match.containsKey("publication_date")) {
      Object publicationDateObject = match.get("publication_date");
      if (publicationDateObject instanceof String) {
        String publicationDate = publicationDateObject.toString();
        this.setPublicationDate(publicationDate);
      }
    }

    if (match.containsKey("doi")) {
      Object doiObject = match.get("doi");
      if (doiObject instanceof String) {
        String doi = doiObject.toString();
        doi = this.preProcessDOI(doi);
        this.setDoi(doi);
      }
    }
    if (match.containsKey("cited_by_count")) {
      int citationCount = (int) match.get("cited_by_count");
      this.setCitationCount(citationCount);
    }
    if (match.containsKey("is_retracted")) {
      Object isRetractedObject = match.get("is_retracted");
      if (isRetractedObject instanceof Boolean) {
        boolean isRetracted = (boolean) isRetractedObject;
        this.setIsRetracted(isRetracted);
      }
    }
    if (match.containsKey("open_access")) {
      Object openAccessObject = match.get("open_access");
      if (openAccessObject instanceof Map<?, ?>) {
        Map<String, Object> openAccess = (Map<String, Object>) openAccessObject;
        if (openAccess.containsKey("is_oa")) {
          Object isOAObject = openAccess.get("is_oa");
          if (isOAObject instanceof Boolean) {
            boolean isOA = (boolean) isOAObject;
            this.setIsOpenAccess(isOA);
          }
        }
      }
    }
  }

  private String preProcessTitleDatabase(String title) {
    String patternStr = "[A-Z][A-Z]+";
    TextHelperService textHelperService = new TextHelperService();
    List<TextRange> indexes = textHelperService.getIndexesOfPatternInText(title, patternStr);
    List<StringInsert> stringInserts = new LinkedList<>();

    for (TextRange textRange : indexes) {
      int start = textRange.getStart();
      int end = textRange.getEnd();
      String before = "";
      String after = "";
      if (start > 0) {
        before = title.substring(start - 1, start);
      }
      if (end < title.length() - 1) {
        after = title.substring(end, end + 1);
      }
      if (!(before.equals("{") && after.equals("}"))) {
        stringInserts.add(new StringInsert(start, "{"));
        stringInserts.add(new StringInsert(end, "}"));
      }
    }
    Collections.sort(
        stringInserts,
        new Comparator<StringInsert>() {
          @Override
          public int compare(StringInsert o1, StringInsert o2) {
            return o2.getIndex() - o1.getIndex();
          }
        });
    for (StringInsert stringInsert : stringInserts) {
      title =
          title.substring(0, stringInsert.getIndex())
              + stringInsert.getToInsert()
              + title.substring(stringInsert.getIndex());
    }
    return title;
  }

  private String preProcessDOI(String doi) {
    if (doi.toLowerCase().contains("doi.org/")) {
      int index = doi.lastIndexOf("doi.org/");
      doi = doi.substring(index + 8);
    }
    return doi;
  }

  private String getAbstractText() {
    return this.abstractText;
  }

  public void setAbstractText(String abstractText) {
    this.abstractText = abstractText;
  }

  private String preprocessPages(String pages, BibTexPagePreprocessing type) {
    if (type == BibTexPagePreprocessing.NO_PREPROCESSING) {
      return pages;
    } else {
      pages = pages.replace("–", "-");
      if (pages.contains("-")) {
        int indexFirstMinus = pages.indexOf("-");
        int indexLastMinus = pages.lastIndexOf("-");
        String startPage = pages.substring(0, indexFirstMinus).trim();
        String endPage = pages.substring(indexLastMinus + 1).trim();
        if (startPage.equals(endPage)) {
          return startPage;
        }
        if (type == BibTexPagePreprocessing.DOUBLE_MINUS_WITH_SPACES) {
          return startPage + " -- " + endPage;
        } else {
          if (type == BibTexPagePreprocessing.DOUBLE_MINUS_WITHOUT_SPACES) {
            return startPage + "--" + endPage;
          } else {
            if (type == BibTexPagePreprocessing.SINGLE_MINUS_WITH_SPACES) {
              return startPage + " - " + endPage;
            } else {
              if (type == BibTexPagePreprocessing.SINGLE_MINUS_WITHOUT_SPACES) {
                return startPage + "-" + endPage;
              } else {
                return pages;
              }
            }
          }
        }
      } else {
        return pages;
      }
    }
  }

  public void processDBLP(
      Map<String, Object> match, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (id != null) {
      this.setId(id);
    }

    if (match.containsKey("info")) {
      Map<String, Object> objectResultsInfo = (Map<String, Object>) match.get("info");
      if (objectResultsInfo.containsKey("authors")) {
        Map<String, Object> objectResultsAuthor =
            (Map<String, Object>) objectResultsInfo.get("authors");
        if (objectResultsAuthor.containsKey("author")) {
          try {
            ArrayList<Map<String, Object>> authorList =
                (ArrayList<Map<String, Object>>) objectResultsAuthor.get("author");
            for (Map<String, Object> authorMap : authorList) {
              if (authorMap.containsKey("text")) {
                String authorName = authorMap.get("text").toString();
                authorName = authorName.replaceAll("\\d", "");
                addAuthor(authorName);
              }
            }
          } catch (ClassCastException e) {
            Map<String, Object> authorMap = (Map<String, Object>) objectResultsAuthor.get("author");
            String authorName = authorMap.get("text").toString();
            authorName = authorName.replaceAll("\\d", "");
            addAuthor(authorName);
          }
        }
      }
      if (objectResultsInfo.containsKey("venue")) {
        String venue = objectResultsInfo.get("venue").toString();
        this.setJournal(venue);
      }
      if (objectResultsInfo.containsKey("access")) {
        String access = objectResultsInfo.get("access").toString();
        if (access.equalsIgnoreCase("open")) {
          this.setIsOpenAccess(true);
        } else {
          if (access.equalsIgnoreCase("closed")) {
            this.setIsOpenAccess(false);
          }
        }
      }
      if (objectResultsInfo.containsKey("volume")) {
        String volume = objectResultsInfo.get("volume").toString();
        this.setVolume(volume);
      }
      if (objectResultsInfo.containsKey("pages")) {
        String pages = objectResultsInfo.get("pages").toString();
        this.setPages(pages, bibTexPagePreprocessing);
      }
      if (objectResultsInfo.containsKey("year")) {
        String year = objectResultsInfo.get("year").toString();
        this.setYear(year);
      }
      if (objectResultsInfo.containsKey("publisher")) {
        this.publisher = objectResultsInfo.get("publisher").toString();
      }
      if (objectResultsInfo.containsKey("doi")) {
        String doi = objectResultsInfo.get("doi").toString();
        this.setDoi(doi);
      }
      if (objectResultsInfo.containsKey("type")) {
        String type = objectResultsInfo.get("type").toString();
        if (type.equalsIgnoreCase("Conference")
            || type.equalsIgnoreCase("Conference and Workshop Papers")) {
          this.setPublicationType("inproceedings");
        } else {
          if (type.equalsIgnoreCase("JournalArticle")
              || type.equalsIgnoreCase("Journal Articles")) {
            this.setPublicationType("article");
          } else {
            if (type.equalsIgnoreCase("Books and Theses") || type.equalsIgnoreCase("Editorship")) {
              this.setPublicationType("book");
            } else {
              this.setPublicationType("misc");
            }
          }
        }
      }
      if (objectResultsInfo.containsKey("title")) {
        String title = objectResultsInfo.get("title").toString();
        title = preProcessTitle(title);
        title = preProcessTitleDatabase(title);
        this.setTitleCorrect(title);
      }
    }
  }

  private Boolean getIsOpenAccess() {
    return this.isOpenAccess;
  }

  private void setIsOpenAccess(boolean isOpenAccess) {
    this.isOpenAccess = isOpenAccess;
  }

  private Boolean getIsRetracted() {
    return this.isRetracted;
  }

  public void setIsRetracted(boolean isRetracted) {
    this.isRetracted = isRetracted;
  }

  private String preProcessTitle(String title) {
    if (title.endsWith(".")) {
      title = title.substring(0, title.length() - 1);
    }
    title = title.replace("&apos;", "");
    title = title.replace("&quot;", "");
    title = title.replace("“", "");
    title = title.replace("”", "");
    title = title.replace("\"", "");
    title = title.replace("[", "");
    title = title.replace("]", "");
    title = title.replace("}", "");
    title = title.replace("{", "");
    return title;
  }

  public String getIssue() {
    return issue;
  }

  public void setIssue(String issue) {
    this.issue = issue;
  }

  public Map<String, String> toMap(boolean abbreviate) {
    Map<String, String> map = new LinkedHashMap<>();
    if (!this.publicationType.equals("")) {
      map.put("type", publicationType);
    }
    if (!this.issue.equals("")) {
      map.put("number", issue);
    }
    if (!this.titleCorrect.equals("")) {
      map.put("title", titleCorrect);
    }
    if (!this.doi.equals("")) {
      map.put("doi", doi);
    }
    if (!this.volume.equals("")) {
      if (publicationType.equalsIgnoreCase("book")) {
        map.put("edition", volume);
      } else {
        map.put("volume", volume);
      }
    }
    if (!this.journal.equals("")) {
      if (publicationType.equalsIgnoreCase("article")) {
        map.put("journal", journal);
      } else {
        if (publicationType.equalsIgnoreCase("inproceedings")
            || publicationType.equalsIgnoreCase("incollection")
            || publicationType.equalsIgnoreCase("inbook")) {
          map.put("booktitle", journal);
        }
      }
    }
    if (this.authors.size() != 0) {
      String authorString = getAuthorsString(abbreviate);

      map.put("author", authorString);
    }
    if (this.editor.size() != 0) {
      String authorString = getEditorsString(abbreviate);

      map.put("editor", authorString);
    }
    if (!this.pages.equals("")) {
      map.put("pages", pages);
    }
    if (!this.year.equals("")) {
      map.put("year", year);
    }
    if (!this.publisher.equals("")) {
      map.put("publisher", publisher);
    }
    if (!this.ISSN.equals("")) {
      map.put("issn", ISSN);
    }
    if (!this.ISBN.equals("")) {
      map.put("isbn", ISBN);
    }
    if (!this.publisherLocation.equals("")) {
      map.put("publisherLocation", publisherLocation);
    }

    map.put("latexString", toLatexString(abbreviate));
    return map;
  }

  public Map<String, Object> toSimpleMap(boolean abbreviate) {
    Map<String, Object> map = new LinkedHashMap<>();
    if (!this.publicationType.equals("")) {
      map.put("type", publicationType);
    }
    if (!this.issue.equals("")) {
      map.put("issue", issue);
    }
    if (!this.titleCorrect.equals("")) {
      map.put("title", titleCorrect);
    }
    if (!this.doi.equals("")) {
      map.put("doi", doi);
    }
    if (!this.volume.equals("")) {
      map.put("volume", volume);
    }
    if (!this.journal.equals("")) {
      map.put("venue", journal);
    }
    if (this.authors.size() != 0) {
      String authorString = getAuthorsString(abbreviate);

      map.put("authorString", authorString);
      map.put("authors", getAuthors());
    }
    if (this.editor.size() != 0) {
      String authorString = getEditorsString(abbreviate);

      map.put("editor", authorString);
    }
    if (!this.pages.equals("")) {
      map.put("pages", pages);
    }
    if (this.licenses.size() != 0) {
      map.put("licenses", getLicenses());
    }
    if (!this.year.equals("")) {
      map.put("year", year);
    }

    if (!this.publisher.equals("")) {
      map.put("publisher", publisher);
    }
    if (!this.ISSN.equals("")) {
      map.put("issn", ISSN);
    }
    if (!this.ISBN.equals("")) {
      map.put("isbn", ISBN);
    }
    if (!this.publisherLocation.equals("")) {
      map.put("publisherLocation", publisherLocation);
    }
    if (!this.publicationDate.equals("")) {
      map.put("publicationDate", publicationDate);
    }
    if (!this.getAbstractText().equals("")) {
      map.put("abstract", this.getAbstractText());
    }
    if (this.getIsRetracted() != null) {
      map.put("isRetracted", getIsRetracted());
    }
    if (this.getIsOpenAccess() != null) {
      map.put("isOpenAccess", getIsOpenAccess());
    }
    if (this.getCitationCount() != -1) {
      map.put("citationCount", getCitationCount());
    }
    if (this.getHighlyInfluentialCitationCount() != -1) {
      map.put("highlyInfluentialCitationCount", getHighlyInfluentialCitationCount());
    }
    map.put("latexString", toLatexString(abbreviate));
    return map;
  }

  public ArrayList<String> getSurnames() {
    ArrayList<String> surnames = new ArrayList<>();

    for (BibTexAuthor author : this.authors) {
      surnames.add(author.getLastName());
    }
    return surnames;
  }

  public String getAuthorsString(boolean abbreviate) {
    String authorString = "";
    for (BibTexAuthor author : authors) {
      if (authorString.equals("")) {
        authorString = author.toBibTexAuthorString(abbreviate);
      } else {
        authorString =
            authorString.trim() + " and " + author.toBibTexAuthorString(abbreviate).trim();
      }
    }
    return authorString;
  }

  public String getEditorsString(boolean abbreviate) {
    String authorString = "";
    for (BibTexAuthor author : editor) {
      if (authorString.equals("")) {
        authorString = author.toBibTexAuthorString(abbreviate);
      } else {
        authorString =
            authorString.trim() + " and " + author.toBibTexAuthorString(abbreviate).trim();
      }
    }
    return authorString;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi.toLowerCase();
  }

  public String getTitleCorrect() {
    return titleCorrect;
  }

  public void setTitleCorrect(String titleCorrect) {
    this.titleCorrect = titleCorrect;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getJournal() {
    return journal;
  }

  public void setJournal(String journal) {
    this.journal = journal;
  }

  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume = volume;
  }

  public String getPages() {
    return pages;
  }

  public void setPages(String pages) {
    this.pages = pages;
  }

  public void setPages(String pages, BibTexPagePreprocessing type) {
    pages = preprocessPages(pages, type);
    this.pages = pages;
  }

  public ArrayList<BibTexAuthor> getAuthors() {
    return authors;
  }

  public void setAuthors(BibTexAuthor[] authors) {
    this.authors.addAll(List.of(authors));
  }

  public void addAuthor(String firstName, String lastName) {
    BibTexAuthor author = new BibTexAuthor(firstName, lastName);
    this.authors.add(author);
  }

  public void addAuthor(String authorString) {
    BibTexAuthor author = new BibTexAuthor(authorString);
    this.authors.add(author);
  }

  public void addEditor(String firstName, String lastName) {
    BibTexAuthor author = new BibTexAuthor(firstName, lastName);
    this.editor.add(author);
  }

  public void addEditor(String authorString) {
    BibTexAuthor author = new BibTexAuthor(authorString);
    this.editor.add(author);
  }

  public ArrayList<BibTexAuthor> getEditor() {
    return editor;
  }

  public void setEditor(BibTexAuthor[] editors) {
    this.editor.addAll(List.of(editors));
  }

  public String getISSN() {
    return ISSN;
  }

  public void setISSN(String ISSN) {
    this.ISSN = ISSN;
  }

  public String getISBN() {
    return ISBN;
  }

  public void setISBN(String ISBN) {
    this.ISBN = ISBN;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public String getPublisherLocation() {
    return publisherLocation;
  }

  public void setPublisherLocation(String publisherLocation) {
    this.publisherLocation = publisherLocation;
  }

  public int getCitationCount() {
    return citationCount;
  }

  public void setCitationCount(int citationCount) {
    this.citationCount = citationCount;
  }

  public int getHighlyInfluentialCitationCount() {
    return highlyInfluentialCitationCount;
  }

  public void setHighlyInfluentialCitationCount(int highlyInfluentialCitationCount) {
    this.highlyInfluentialCitationCount = highlyInfluentialCitationCount;
  }

  public String getPublicationType() {
    return publicationType;
  }

  public void setPublicationType(String publicationType) {
    this.publicationType = publicationType;
  }

  public String toLatexStringBraces(boolean abbreviateNames) {
    if (this.publicationType.equalsIgnoreCase("article")) {
      String res = "@article\\{" + id + ",\n";
      if (!titleCorrect.equals("")) {
        res = res + "title=\\{" + titleCorrect + "\\}, \n";
      }
      if (authors.size() != 0) {

        res = res + "author=\\{" + getAuthorsString(abbreviateNames) + "\\}, \n";
      }
      if (editor.size() != 0) {

        res = res + "editor=\\{" + getEditorsString(abbreviateNames) + "\\}, \n";
      }
      if (!journal.equals("")) {
        res = res + "journal=\\{" + journal + "\\}, \n";
      }
      if (!volume.equals("")) {
        res = res + "volume=\\{" + volume + "\\}, \n";
      }
      if (!issue.equals("")) {
        res = res + "number=\\{" + issue + "\\}, \n";
      }
      if (!year.equals("")) {
        res = res + "year=\\{" + year + "\\}, \n";
      }
      if (!pages.equals("")) {
        res = res + "pages=\\{" + pages + "\\}, \n";
      }
      if (!doi.equals("")) {
        res = res + "doi=\\{" + doi + "\\}, \n";
      }
      if (res.endsWith(", \n")) {
        res = res.substring(0, res.length() - 3);
      }
      res = res + "\\}";
      return res;
    } else {
      if (this.publicationType.equalsIgnoreCase("inproceedings")) {
        String res = "@inProceedings\\{" + id + ",\n";
        if (!titleCorrect.equals("")) {
          res = res + "title=\\{" + titleCorrect + "\\}, \n";
        }
        if (authors.size() != 0) {
          res = res + "author=\\{" + getAuthorsString(abbreviateNames) + "\\}, \n";
        }
        if (editor.size() != 0) {

          res = res + "editor=\\{" + getEditorsString(abbreviateNames) + "\\}, \n";
        }
        if (!journal.equals("")) {
          res = res + "booktitle=\\{" + journal + "\\}, \n";
        }
        if (!volume.equals("")) {
          res = res + "volume=\\{" + volume + "\\}, \n";
        }
        if (!issue.equals("")) {
          res = res + "number=\\{" + issue + "\\}, \n";
        }
        if (!year.equals("")) {
          res = res + "year=\\{" + year + "\\}, \n";
        }
        if (!pages.equals("")) {
          res = res + "pages=\\{" + pages + "\\}, \n";
        }
        if (!doi.equals("")) {
          res = res + "doi=\\{" + doi + "\\}, \n";
        }

        if (res.endsWith(", \n")) {
          res = res.substring(0, res.length() - 3);
        }
        res = res + "\\}";
        return res;
      } else {
        String res = "@Misc\\{" + id + ",\n";
        if (!titleCorrect.equals("")) {
          res = res + "title=\\{" + titleCorrect + "\\}, \n";
        }
        if (authors.size() != 0) {
          res = res + "author=\\{" + getAuthorsString(abbreviateNames) + "\\}, \n";
        }
        if (editor.size() != 0) {

          res = res + "editor=\\{" + getEditorsString(abbreviateNames) + "\\}, \n";
        }
        if (!journal.equals("")) {
          res = res + "howpublished=\\{" + journal + "\\}, \n";
        }
        if (!volume.equals("")) {
          res = res + "version=\\{" + volume + "\\}, \n";
        }
        if (!year.equals("")) {
          res = res + "year=\\{" + year + "\\}, \n";
        }
        if (!pages.equals("")) {
          res = res + "note=\\{" + pages + "\\}, \n";
        }
        if (!doi.equals("")) {
          res = res + "doi=\\{" + doi + "\\}, \n";
        }
        if (res.endsWith(", \n")) {
          res = res.substring(0, res.length() - 3);
        }
        res = res + "\\}";

        return res;
      }
    }
  }

  private void processSemanticScholarData(
      Map<String, Object> jsonObject, String id, BibTexPagePreprocessing bibTexPreProcessing) {
    if (id != null) {
      if (!id.equals("")) {
        this.setId(id);
      }
    }
    if (jsonObject == null) {
      return;
    }
    Map<String, Object> mapInfo = jsonObject;
    this.id = id;

    if (mapInfo.containsKey("externalIds")) {
      if (mapInfo.get("externalIds") != null) {
        Map<String, Object> externalIds = (Map<String, Object>) mapInfo.get("externalIds");
        if (externalIds.containsKey("DOI")) {
          if (externalIds.get("DOI") != null) {
            String doi = (String) externalIds.get("DOI");
            doi = preProcessDOI(doi);
            this.setDoi(doi);
          }
        }
      }
    }
    if (mapInfo.containsKey("title")) {
      if (mapInfo.get("title") != null) {
        String titleCorrect = (String) mapInfo.get("title");
        titleCorrect = preProcessTitle(titleCorrect);
        titleCorrect = preProcessTitleDatabase(titleCorrect);
        this.setTitleCorrect(titleCorrect);
      }
    }
    if (mapInfo.containsKey("abstract")) {
      if (mapInfo.get("abstract") != null) {
        String abstractText = (String) mapInfo.get("abstract");
        this.setAbstractText(abstractText);
      }
    }
    if (mapInfo.containsKey("isOpenAccess")) {
      if (mapInfo.get("isOpenAccess") != null) {
        boolean isOpenAccess = (boolean) mapInfo.get("isOpenAccess");
        this.setIsOpenAccess(isOpenAccess);
      }
    }
    if (mapInfo.containsKey("year")) {
      if (mapInfo.get("year") != null) {
        String year = mapInfo.get("year").toString();

        this.setYear(year);
      }
    } else {
      if (mapInfo.containsKey("publicationDate")) {
        if (mapInfo.get("publicationDate") != null) {
          String publicationDate = mapInfo.get("publicationDate").toString();
          this.setPublicationDate(publicationDate);
        }
      }
    }
    if (mapInfo.containsKey("journal")) {
      if (mapInfo.get("journal") != null) {
        Map<String, Object> journalInfo = (Map<String, Object>) mapInfo.get("journal");
        if (journalInfo != null) {
          if (journalInfo.containsKey("name")) {
            if (journalInfo.get("name") != null) {
              String name = (String) journalInfo.get("name");
              this.setJournal(name);
            }
          }
          if (journalInfo.containsKey("pages")) {
            if (journalInfo.get("pages") != null) {
              String pages = (String) journalInfo.get("pages");
              this.setPages(pages, bibTexPreProcessing);
            }
          }
          if (journalInfo.containsKey("volume")) {
            if (journalInfo.get("volume") != null) {
              String volume = (String) journalInfo.get("volume");
              this.setVolume(volume);
            }
          }
        }
      }
    }
    if (this.getJournal().strip().equals("")) {
      if (mapInfo.containsKey("venue")) {
        if (mapInfo.get("venue") != null) {
          String venue = (String) mapInfo.get("venue");

          this.setJournal(venue);
        }
      }
    }
    if (mapInfo.containsKey("authors")) {
      if (mapInfo.get("authors") != null) {
        ArrayList<Map<String, Object>> authorsNames =
            (ArrayList<Map<String, Object>>) mapInfo.get("authors");
        for (Map<String, Object> authorMap : authorsNames) {
          if (authorMap.containsKey("name")) {
            String authorName = (String) authorMap.get("name");
            authorName = authorName.replaceAll("\\d", "");
            this.addAuthor(authorName);
          }
        }
      }
    }
    if (mapInfo.containsKey("editors")) {
      if (mapInfo.get("editors") != null) {
        ArrayList<Map<String, Object>> authorsNames =
            (ArrayList<Map<String, Object>>) mapInfo.get("editors");
        for (Map<String, Object> authorMap : authorsNames) {
          if (authorMap.containsKey("name")) {
            String authorName = (String) authorMap.get("name");
            authorName = authorName.replaceAll("\\d", "");
            this.addEditor(authorName);
          }
        }
      }
    }
    if (mapInfo.containsKey("publicationTypes")) {
      if (mapInfo.get("publicationTypes") != null) {
        ArrayList<String> publicationTypes = (ArrayList<String>) mapInfo.get("publicationTypes");
        if (publicationTypes == null) {
          setPublicationType("misc");
        } else {
          if (publicationTypes.contains("Conference")
              || publicationTypes.contains("Conference and Workshop Papers")) {
            this.setPublicationType("inproceedings");
          } else {
            if (publicationTypes.contains("JournalArticle")
                || publicationTypes.contains("Journal Articles")) {
              this.setPublicationType("article");
            } else {
              this.setPublicationType("misc");
            }
          }
        }
      } else {
        this.setPublicationType("misc");
      }
    } else {
      this.setPublicationType("misc");
    }
    if (mapInfo.containsKey("citationCount")) {
      if (mapInfo.get("citationCount") != null) {
        this.citationCount = (int) mapInfo.get("citationCount");
      }
    }
    if (mapInfo.containsKey("influentialCitationCount")) {
      if (mapInfo.get("influentialCitationCount") != null) {
        this.highlyInfluentialCitationCount = (int) mapInfo.get("influentialCitationCount");
      }
    }
  }

  public String[] extractRelevantFields(Map<String, String[]> mapping) {
    String[] relevantFields = null;

    if (!this.getPublicationType().equals("")) {
      relevantFields = mapping.get(this.getPublicationType().toLowerCase());
    } else {
      relevantFields = mapping.get("default");
    }
    if (relevantFields == null) {
      relevantFields = mapping.get("default");
    }
    return relevantFields;
  }

  public String toLatexString(boolean abbreviateNames) {
    if (this.publicationType.equalsIgnoreCase("article")) {
      String res = "@Article{" + id + ",\n";
      if (!titleCorrect.equals("")) {
        res = res + "\t title={" + titleCorrect + "}, \n";
      }
      if (authors.size() != 0) {
        res = res + "\t author={" + getAuthorsString(abbreviateNames) + "}, \n";
      }
      if (!journal.equals("")) {
        res = res + "\t journal={" + journal + "}, \n";
      }
      if (!volume.equals("")) {
        res = res + "\t volume={" + volume + "}, \n";
      }
      if (!issue.equals("")) {
        res = res + "\t number={" + issue + "}, \n";
      }
      if (!year.equals("")) {
        res = res + "\t year={" + year + "}, \n";
      }
      if (!pages.equals("")) {
        res = res + "\t pages={" + pages + "}, \n";
      }
      if (!doi.equals("")) {
        res = res + "\t doi={" + doi + "}, \n";
      }
      if (res.endsWith(", \n")) {
        res = res.substring(0, res.length() - 3);
      }
      res = res + "\n}";
      return res;
    } else {
      if (this.publicationType.contains("inproceedings")) {
        String res = "@InProceedings{" + id + ",\n";
        if (!titleCorrect.equals("")) {
          res = res + "\t title={" + titleCorrect + "}, \n";
        }
        if (authors.size() != 0) {
          res = res + "\t author={" + getAuthorsString(abbreviateNames) + "}, \n";
        }
        if (!journal.equals("")) {
          res = res + "\t booktitle={" + journal + "}, \n";
        }
        if (!volume.equals("")) {
          res = res + "\t volume={" + volume + "}, \n";
        }
        if (!issue.equals("")) {
          res = res + "\t number={" + issue + "}, \n";
        }
        if (!year.equals("")) {
          res = res + "\t year={" + year + "}, \n";
        }
        if (!pages.equals("")) {
          res = res + "\t pages={" + pages + "}, \n";
        }
        if (!doi.equals("")) {
          res = res + "\t doi={" + doi + "}, \n";
        }

        if (res.endsWith(", \n")) {
          res = res.substring(0, res.length() - 3);
        }
        res = res + "\n}";
        return res;
      } else {

        if (this.publicationType.equalsIgnoreCase("book")) {
          String res = "@book{" + id + ",\n";
          if (!titleCorrect.equals("")) {
            res = res + "\t title={" + titleCorrect + "}, \n";
          }
          if (authors.size() != 0) {
            res = res + "\t author={" + getAuthorsString(abbreviateNames) + "}, \n";
          }
          if (editor.size() != 0) {

            res = res + "\t editor=\\{" + getEditorsString(abbreviateNames) + "\\}, \n";
          }
          if (!volume.equals("")) {
            res = res + "\t edition={" + volume + "}, \n";
          }
          if (!year.equals("")) {
            res = res + "\t year={" + year + "}, \n";
          }
          if (!pages.equals("")) {
            res = res + "\t pages={" + pages + "}, \n";
          }
          if (!doi.equals("")) {
            res = res + "\t doi={" + doi + "}, \n";
          }
          if (!publisher.equals("")) {
            res = res + "\t publisher={" + publisher + "}, \n";
          }
          if (!publisherLocation.equals("")) {
            res = res + "\t address={" + publisherLocation + "}, \n";
          }
          if (!doi.equals("")) {
            res = res + "\t doi={" + doi + "}, \n";
          }
          if (!ISBN.equals("")) {
            res = res + "\t isbn={" + ISBN + "}, \n";
          }
          if (res.endsWith(", \n")) {
            res = res.substring(0, res.length() - 3);
          }
          res = res + "\n}";
          return res;
        } else {
          String res = "@Misc{" + id + ",\n";
          if (!titleCorrect.equals("")) {
            res = res + "\t title={" + titleCorrect + "}, \n";
          }
          if (authors.size() != 0) {
            res = res + "\t author={" + getAuthorsString(abbreviateNames) + "}, \n";
          }
          if (editor.size() != 0) {

            res = res + "\t editor=\\{" + getEditorsString(abbreviateNames) + "\\}, \n";
          }
          if (!journal.equals("")) {
            res = res + "\t howpublished={" + journal + "}, \n";
          }
          if (!volume.equals("")) {
            res = res + "\t version={" + volume + "}, \n";
          }
          if (!year.equals("")) {
            res = res + "\t year={" + year + "}, \n";
          }
          if (!pages.equals("")) {
            res = res + "\t note={" + pages + "}, \n";
          }
          if (!doi.equals("")) {
            res = res + "\t doi={" + doi + "}, \n";
          }
          if (res.endsWith(", \n")) {
            res = res.substring(0, res.length() - 3);
          }
          res = res + "\n}";

          return res;
        }
      }
    }
  }

  private void processCrossCiteObject(
      Map<String, Object> jsonObject, String id, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (id != null) {
      if (!id.equals("")) {
        this.setId(id);
      }
    }

    if (jsonObject == null) {
      return;
    }
    Map<String, Object> mapInfo = jsonObject;
    if (mapInfo.containsKey("type")) {
      String type = (String) mapInfo.get("type");
      if (type.equalsIgnoreCase("book-chapter") || type.equalsIgnoreCase("proceedings-article")) {
        this.setPublicationType("inproceedings");
      } else {
        if (type.equalsIgnoreCase("journal-article")) {
          this.setPublicationType("article");
        } else {
          if (type.equalsIgnoreCase("book")) {
            this.setPublicationType("book");
          } else {
            if (type.equalsIgnoreCase("proceedings")) {
              this.setPublicationType("proceedings");
            } else {
              this.setPublicationType("misc");
            }
          }
        }
      }
    }
    if (mapInfo.containsKey("DOI")) {
      String doi = (String) mapInfo.get("DOI");
      doi = preProcessDOI(doi);
      this.setDoi(doi);
    }
    if (mapInfo.containsKey("ISSN")) {
      this.ISSN = String.join(", ", (ArrayList<String>) mapInfo.get("ISSN"));
    }
    if (mapInfo.containsKey("ISBN")) {
      this.ISBN = String.join(", ", (ArrayList<String>) mapInfo.get("ISBN"));
    }
    if (mapInfo.containsKey("publisher")) {
      this.publisher = (String) mapInfo.get("publisher");
    }
    if (mapInfo.containsKey("license")) {
      ArrayList<Map<String, Object>> licensesList =
          (ArrayList<Map<String, Object>>) mapInfo.get("license");
      for (Map<String, Object> licenseInformationMap : licensesList) {
        if (licenseInformationMap.containsKey("URL")) {
          Object licenseURLObject = licenseInformationMap.get("URL");
          if (licenseURLObject instanceof String) {
            String licenseURL = licenseURLObject.toString();
            this.addLicense(licenseURL);
          }
        }
      }
    }
    if (mapInfo.containsKey("abstract")) {
      this.abstractText = (String) mapInfo.get("abstract");
    }
    if (mapInfo.containsKey("is-referenced-by-count")) {
      Object referencedByCountObject = mapInfo.get("is-referenced-by-count");
      if (referencedByCountObject instanceof Integer) {
        int referencedByCount = (int) referencedByCountObject;
        this.setCitationCount(referencedByCount);
      }
    }
    if (mapInfo.containsKey("publisher-location")) {
      this.publisherLocation = (String) mapInfo.get("publisher-location");
    }
    if (mapInfo.containsKey("volume")) {
      this.setVolume(mapInfo.get("volume").toString());
    }
    if (mapInfo.containsKey("container-title")) {
      String journal = mapInfo.get("container-title").toString();
      if (journal.startsWith("[")) {
        journal = journal.substring(1);
      }
      if (journal.endsWith("]")) {
        journal = journal.substring(0, journal.length() - 1);
      }
      this.setJournal(journal);
    }
    if (mapInfo.containsKey("journal-issue")) {
      Map<String, Object> issueInfo = (Map<String, Object>) mapInfo.get("journal-issue");
      if (issueInfo.containsKey("issue")) {
        this.setIssue(issueInfo.get("issue").toString());
      }
    }
    if (mapInfo.containsKey("page")) {

      this.setPages(mapInfo.get("page").toString(), bibTexPagePreprocessing);
    }
    if (mapInfo.containsKey("title")) {
      String title = mapInfo.get("title").toString();
      title = preProcessTitle(title);
      title = preProcessTitleDatabase(title);
      this.setTitleCorrect(title);
    }
    if (mapInfo.containsKey("subtitle")) {
      ArrayList<String> subtitles = (ArrayList<String>) mapInfo.get("subtitle");

      if (subtitles.size() > 0) {
        String subtitle = StringUtils.join(subtitles, " ");
        subtitle = preProcessTitle(subtitle);
        subtitle = preProcessTitleDatabase(subtitle);
        this.setTitleCorrect(this.getTitleCorrect() + " " + subtitle);
      }
    }
    if (mapInfo.containsKey("author")) {
      if (!this.publicationType.equalsIgnoreCase("proceedings")) {
        ArrayList<Object> authorInfo = (ArrayList<Object>) mapInfo.get("author");
        for (Object authorObject : authorInfo) {
          Map<String, Object> authorMap = (Map<String, Object>) authorObject;
          if (authorMap.containsKey("given") && authorMap.containsKey("family")) {
            addAuthor((String) authorMap.get("given"), (String) authorMap.get("family"));
          } else {
            if (authorMap.containsKey("family")) {
              addAuthor((String) authorMap.get("family"));
            }
          }
        }
      } else {
        ArrayList<Object> authorInfo = (ArrayList<Object>) mapInfo.get("author");
        for (Object authorObject : authorInfo) {
          Map<String, Object> authorMap = (Map<String, Object>) authorObject;
          if (authorMap.containsKey("given") && authorMap.containsKey("family")) {
            addEditor((String) authorMap.get("given"), (String) authorMap.get("family"));
          } else {
            if (authorMap.containsKey("family")) {
              addEditor((String) authorMap.get("family"));
            }
          }
        }
      }
    }

    if (mapInfo.containsKey("editor")) {
      ArrayList<Object> authorInfo = (ArrayList<Object>) mapInfo.get("editor");
      for (Object authorObject : authorInfo) {
        Map<String, Object> authorMap = (Map<String, Object>) authorObject;
        if (authorMap.containsKey("given") && authorMap.containsKey("family")) {
          addEditor((String) authorMap.get("given"), (String) authorMap.get("family"));
        } else {
          if (authorMap.containsKey("family")) {
            addEditor((String) authorMap.get("family"));
          }
        }
      }
    }

    if (mapInfo.containsKey("published")) {
      Map<String, Object> publishedInfo = (Map<String, Object>) mapInfo.get("published");
      if (publishedInfo.containsKey("date-parts")) {
        ArrayList<Object> dateList = (ArrayList<Object>) publishedInfo.get("date-parts");
        if (dateList.size() > 0) {
          ArrayList<Integer> dateListNested = (ArrayList<Integer>) dateList.get(0);
          if (dateListNested.size() > 0) {
            this.setYear(String.valueOf(dateListNested.get(0)));
          }
        }
      }
    }
  }

  public boolean isArxiv(ArxivHelper textHelperService) {
    if (textHelperService.checkArxivInText(journal)) {
      return true;
    } else {
      if (textHelperService.checkArxivInText(volume)) {
        return true;
      } else {
        if (textHelperService.checkArxivInText(doi)) {
          return true;
        } else {
          if (textHelperService.checkArxivInText(issue)) {
            return true;
          } else {
            return textHelperService.checkArxivInText(publisher);
          }
        }
      }
    }
  }
}
