package de.ippolis.wp3.literature_database_service.openalex.service;

import de.ippolis.wp3.literature_database_service.utils.helper.BibTexHelperService;
import de.ippolis.wp3.literature_database_service.utils.model.AbstractLiteratureAnalyzer;
import de.ippolis.wp3.literature_database_service.utils.model.ArxivHelper;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexReferenceComparisonHelper;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import java.util.LinkedList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class OpenAlexLiteratureAnalyzer extends AbstractLiteratureAnalyzer {

  private final OpenAlexLocalDatabaseService openAlexLocalDatabaseService;
  private final OpenAlexAPIReferenceService openAlexAPIReferenceService;

  public OpenAlexLiteratureAnalyzer(
      ArxivHelper arxivHelper,
      BibTexHelperService bibTexHelperService,
      BibTexReferenceComparisonHelper bibTexReferenceComparisonHelper,
      OpenAlexLocalDatabaseService openAlexLocalDatabaseService,
      OpenAlexAPIReferenceService openAlexAPIReferenceService) {
    super(arxivHelper, bibTexHelperService, bibTexReferenceComparisonHelper);
    this.openAlexLocalDatabaseService = openAlexLocalDatabaseService;
    this.openAlexAPIReferenceService = openAlexAPIReferenceService;
  }

  @Override
  protected List<Reference> extractLocalResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    return openAlexLocalDatabaseService.getByDOI(doi, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractLocalResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    return openAlexLocalDatabaseService.getByTitle(title, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractLocalResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (arxivId == null || arxivId.strip().equals("")) {
      return new LinkedList<>();
    }
    return openAlexLocalDatabaseService.getByArxivId(arxivId, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractAPIResultsByTitle(
      String id, String title, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }
    return openAlexAPIReferenceService.getOpenAlexResultsByTitle(
        title, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractAPIResultsByDOI(
      String id, String doi, BibTexPagePreprocessing bibTexPagePreprocessing) {
    if (doi == null || doi.strip().equals("")) {
      return new LinkedList<>();
    }
    return openAlexAPIReferenceService.getOpenAlexResultByDOI(doi, id, bibTexPagePreprocessing);
  }

  @Override
  protected List<Reference> extractAPIResultsByArxivId(
      String id, String arxivId, BibTexPagePreprocessing bibTexPagePreprocessing) {
    /*TODO*/
    return new LinkedList<>();
  }
}
