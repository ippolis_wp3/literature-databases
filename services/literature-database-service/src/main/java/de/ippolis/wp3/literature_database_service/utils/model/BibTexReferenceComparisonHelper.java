package de.ippolis.wp3.literature_database_service.utils.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class BibTexReferenceComparisonHelper {
  private final MessageSource messageSource;

  @Autowired
  public BibTexReferenceComparisonHelper(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  public String preProcessName(String name) {
    name = name.toLowerCase();
    name = name.replace("\\\"u", "ü");
    name = name.replace("\\\"a", "ä");
    name = name.replace("\\\"o", "ö");
    name = name.replace("\\'i", "í");
    name = name.replace("\\'{i}", "í");
    name = name.replace("\\'{e}", "é");
    name = name.replace("\\'e", "é");
    name = name.replace("{", "");
    name = name.replace("}", "");
    return (name);
  }

  private String preProcessAuthorNamesForComparison(String name) {
    name = name.replaceAll("[^a-zA-Z0-9\s]", "");
    name = name.toLowerCase();
    name = name.strip();
    return name;
  }

  public boolean checkAuthorsMatch(List<String> authors, List<String> authorNames) {
    if (authors.size() <= 0) {
      return false;
    }
    int authorsTotal = Math.max(authors.size(), authorNames.size());
    int authorsFound = 0;
    for (String author : authors) {
      for (String authorName : authorNames) {
        if (preProcessAuthorNamesForComparison(authorName)
            .contains(preProcessAuthorNamesForComparison(author))) {
          authorsFound++;
          break;
        }
      }
    }
    return (((double) authorsFound / (double) authorsTotal) > 0.7);
  }

  private String preprocessTitle(String title) {
    title = title.replaceAll("[^a-zA-Z0-9\s]", "");
    title = title.toLowerCase();
    title = title.strip();
    return title;
  }

  public boolean checkTitlesMatch(String title, String comparison) {
    title = preprocessTitle(title);
    comparison = preprocessTitle(comparison);
    int levenshteinDistance =
        StringUtils.getLevenshteinDistance(title.toLowerCase(), comparison.toLowerCase());
    int lettersFound = title.length();
    int letters = comparison.length();
    int maxLetters = Math.max(letters, lettersFound);
    double ratio = (double) levenshteinDistance / (double) maxLetters;
    return (ratio < 0.05);
  }

  private String getValueFromMap(Map<String, String> map, String key) {
    String value = "";
    if (map.containsKey(key)) {
      value = map.get(key);
    }
    return value;
  }

  private String preprocessBraces(String text) {
    text = text.replace("{", "\\{");
    text = text.replace("}", "\\}");
    text = text.replace("_", "\\_");
    text = text.replace("\\\\{", "\\{");
    text = text.replace("\\\\}", "\\}");
    text = text.replace("\\\\_", "\\_");
    return text;
  }

  public double calculateLevenshteinDistanceRatio(String originalTitle, String foundTitle) {
    int levenshteinDistance =
        StringUtils.getLevenshteinDistance(originalTitle.toLowerCase(), foundTitle.toLowerCase());
    int lettersFound = originalTitle.length();
    int letters = foundTitle.length();
    int maxLetters = Math.max(letters, lettersFound);
    double ratio = (double) levenshteinDistance / (double) maxLetters;
    return ratio;
  }

  public double calculateAuthorRatio(List<String> authors, List<String> authorNames) {
    int authorsTotal = Math.max(authors.size(), authorNames.size());
    int authorsFound = 0;
    for (String author : authors) {
      for (String authorName : authorNames) {
        if (preProcessAuthorNamesForComparison(authorName)
            .contains(preProcessAuthorNamesForComparison(author))) {
          authorsFound++;
          break;
        }
      }
    }
    return (double) authorsFound / (double) authorsTotal;
  }

  private String preprocessBibTexField(String text) {
    text = text.toLowerCase();
    // text = text.replace("{", "");
    // text = text.replace("}", "");
    text = text.replace("  ", " ");
    text = text.replace("--", "-");
    text = text.replace("\\_", "_");
    text = text.strip();
    return text;
  }

  public String preprocessDOI(String doi) {
    doi = doi.toLowerCase();
    if (doi.endsWith(".")) {
      doi = doi.substring(0, doi.length() - 1);
    }
    if (doi.contains("doi.org/")) {
      int lastIndex = doi.indexOf("doi.org/");
      doi = doi.substring(lastIndex + 8);
    }
    return doi.strip();
  }

  public List<Reference> filterMatches(
      List<Reference> possibleMatches, List<String> authors, String title) {
    List<Reference> filteredMatches = new LinkedList<>();
    for (Reference possibleMatch : possibleMatches) {
      if (checkTitlesMatch(title, possibleMatch.getTitleCorrect())) {
        ArrayList<String> surnames = possibleMatch.getSurnames();
        if (checkAuthorsMatch(surnames, authors)) {
          filteredMatches.add(possibleMatch);
        }
      }
    }
    return filteredMatches;
  }
}
