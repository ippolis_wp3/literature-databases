package de.ippolis.wp3.literature_database_service.utils.model.dto;

import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import java.util.List;

public class TitleAuthorPageProcessingRequestDTO {
  private String title;
  private List<String> authors;
  private String id;
  private BibTexPagePreprocessing bibTexPagePreprocessing;
  private boolean useLocalData;
  private boolean useAPI;

  public TitleAuthorPageProcessingRequestDTO() {}

  public TitleAuthorPageProcessingRequestDTO(
      String title,
      List<String> authors,
      String id,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      boolean useLocalData,
      boolean useAPI) {
    this.title = title;
    this.authors = authors;
    this.id = id;
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
    this.useLocalData = useLocalData;
    this.useAPI = useAPI;
  }

  public boolean isUseLocalData() {
    return useLocalData;
  }

  public void setUseLocalData(boolean useLocalData) {
    this.useLocalData = useLocalData;
  }

  public boolean isUseAPI() {
    return useAPI;
  }

  public void setUseAPI(boolean useAPI) {
    this.useAPI = useAPI;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }
}
