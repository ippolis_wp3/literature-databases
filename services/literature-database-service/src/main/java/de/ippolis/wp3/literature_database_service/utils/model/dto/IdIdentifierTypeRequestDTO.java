package de.ippolis.wp3.literature_database_service.utils.model.dto;

import de.ippolis.wp3.literature_database_service.semantic_scholar.model.SemanticScholarIdentifierTypes;

public class IdIdentifierTypeRequestDTO {
  private String id;
  private SemanticScholarIdentifierTypes semanticScholarIdentifierTypes;

  public IdIdentifierTypeRequestDTO() {}

  public IdIdentifierTypeRequestDTO(
      String id, SemanticScholarIdentifierTypes semanticScholarIdentifierTypes) {
    this.id = id;
    this.semanticScholarIdentifierTypes = semanticScholarIdentifierTypes;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public SemanticScholarIdentifierTypes getSemanticScholarIdentifierTypes() {
    return semanticScholarIdentifierTypes;
  }

  public void setSemanticScholarIdentifierTypes(
      SemanticScholarIdentifierTypes semanticScholarIdentifierTypes) {
    this.semanticScholarIdentifierTypes = semanticScholarIdentifierTypes;
  }
}
