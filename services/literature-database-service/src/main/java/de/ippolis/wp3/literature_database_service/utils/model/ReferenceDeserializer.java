package de.ippolis.wp3.literature_database_service.utils.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.io.IOException;

public class ReferenceDeserializer extends StdDeserializer<Reference> {
  public ReferenceDeserializer() {
    this(null);
  }

  public ReferenceDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public Reference deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    if (node.get("id") != null) {
      Reference reference = new Reference(node.get("id").asText());
      if (node.get("titleCorrect") != null) {
        reference.setTitleCorrect(node.get("titleCorrect").asText());
      }
      if (node.get("year") != null) {
        reference.setYear(node.get("year").asText());
      }
      if (node.get("journal") != null) {
        reference.setJournal(node.get("journal").asText());
      }
      if (node.get("volume") != null) {
        reference.setVolume(node.get("volume").asText());
      }
      if (node.get("pages") != null) {
        reference.setPages(node.get("pages").asText());
      }
      if (node.get("doi") != null) {
        reference.setDoi(node.get("doi").asText());
      }
      if (node.get("publicationType") != null) {
        reference.setPublicationType(node.get("publicationType").asText());
      }
      if (node.get("issue") != null) {
        reference.setIssue(node.get("issue").asText());
      }
      if (node.get("ISSN") != null) {
        reference.setISSN(node.get("ISSN").asText());
      }
      if (node.get("ISBN") != null) {
        reference.setISBN(node.get("ISBN").asText());
      }
      if (node.get("publisher") != null) {
        reference.setPublisher(node.get("publisher").asText());
      }
      if (node.get("publisherLocation") != null) {
        reference.setPublisherLocation(node.get("publisherLocation").asText());
      }
      if (node.get("citationCount") != null) {
        reference.setCitationCount(node.get("citationCount").asInt());
      }
      if (node.get("highlyInfluentialCitationCount") != null) {
        reference.setHighlyInfluentialCitationCount(
            node.get("highlyInfluentialCitationCount").asInt());
      }
      if (node.get("abstractText") != null) {
        reference.setAbstractText(node.get("abstractText").asText());
      }
      if (node.get("isOpenAccess") != null) {
        reference.setOpenAccess(node.get("isOpenAccess").asBoolean());
      }
      if (node.get("isRetracted") != null) {
        reference.setIsRetracted(node.get("isRetracted").asBoolean());
      }
      if (node.get("publicationDate") != null) {
        reference.setPublicationDate(node.get("publicationDate").asText());
      }
      if (node.get("linkedArxivId") != null) {
        reference.setLinkedArxivId(node.get("linkedArxivId").asText());
      }
      ObjectMapper mapper = new ObjectMapper();
      SimpleModule module = new SimpleModule();
      module.addDeserializer(BibTexAuthor.class, new BibTexAuthorDeserializer());
      mapper.registerModule(module);
      if (node.get("authors") != null) {
        BibTexAuthor[] authors =
            mapper.readValue(node.get("authors").toString(), BibTexAuthor[].class);
        reference.setAuthors(authors);
      }
      if (node.get("editors") != null) {
        BibTexAuthor[] editors =
            mapper.readValue(node.get("editor").toString(), BibTexAuthor[].class);
        reference.setEditor(editors);
      }
      if (node.get("licenses") != null) {
        if (node.get("licenses").isArray()) {
          for (final JsonNode license : node.get("licenses")) {
            reference.addLicense(license.asText());
          }
        }
      }
      return reference;
    }
    return null;
  }
}
