package de.ippolis.wp3.literature_database_service.utils.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

public class BibTexAuthorDeserializer extends StdDeserializer<BibTexAuthor> {
  public BibTexAuthorDeserializer() {
    this(null);
  }

  public BibTexAuthorDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public BibTexAuthor deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    if (node.get("firstName") != null && node.get("lastName") != null) {
      BibTexAuthor author =
          new BibTexAuthor(node.get("firstName").asText(), node.get("lastName").asText());
      return author;
    }
    return null;
  }
}
