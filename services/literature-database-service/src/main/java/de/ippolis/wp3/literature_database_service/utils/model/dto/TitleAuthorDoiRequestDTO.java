package de.ippolis.wp3.literature_database_service.utils.model.dto;

import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import java.util.List;

public class TitleAuthorDoiRequestDTO {
  private String title;
  private List<String> authors;
  private BibTexPagePreprocessing bibTexPagePreprocessing;
  private String doi;
  private boolean useLocalData;
  private boolean useAPI;

  public TitleAuthorDoiRequestDTO() {}

  public TitleAuthorDoiRequestDTO(
      String title,
      List<String> authors,
      BibTexPagePreprocessing bibTexPagePreprocessing,
      String doi,
      boolean useLocalData,
      boolean useAPI) {
    this.title = title;
    this.authors = authors;
    this.doi = doi;
    this.useLocalData = useLocalData;
    this.useAPI = useAPI;
  }

  public BibTexPagePreprocessing getBibTexPagePreprocessing() {
    return bibTexPagePreprocessing;
  }

  public void setBibTexPagePreprocessing(BibTexPagePreprocessing bibTexPagePreprocessing) {
    this.bibTexPagePreprocessing = bibTexPagePreprocessing;
  }

  public boolean isUseLocalData() {
    return useLocalData;
  }

  public void setUseLocalData(boolean useLocalData) {
    this.useLocalData = useLocalData;
  }

  public boolean isUseAPI() {
    return useAPI;
  }

  public void setUseAPI(boolean useAPI) {
    this.useAPI = useAPI;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  public String getDoi() {
    return doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }
}
