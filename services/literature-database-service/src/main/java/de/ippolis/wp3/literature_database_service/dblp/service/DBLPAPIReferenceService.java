package de.ippolis.wp3.literature_database_service.dblp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.ippolis.wp3.literature_database_service.utils.model.BibTexPagePreprocessing;
import de.ippolis.wp3.literature_database_service.utils.model.Reference;
import de.ippolis.wp3.literature_database_service.utils.model.ReferenceDeserializer;
import de.ippolis.wp3.literature_database_service.utils.service.APIRestService;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class DBLPAPIReferenceService {
  private final APIRestService apiRestService;

  public DBLPAPIReferenceService(APIRestService apiRestService) {

    this.apiRestService = apiRestService;
  }

  public List<Reference> getDBLPReferencesByTitle(
      String title, BibTexPagePreprocessing bibTexPagePreprocessing, String id) {
    if (title == null || title.strip().equals("")) {
      return new LinkedList<>();
    }

    JSONObject requestParameters = new JSONObject();
    requestParameters.put("title", title);
    requestParameters.put("bibTexPagePreprocessing", bibTexPagePreprocessing);
    requestParameters.put("id", id);
    final JSONObject response =
        this.apiRestService.performJsonPostRequest(
            "/dblp/getAPIReferencesByTitle", requestParameters);
    if (response.getString("status").equals("SUCCESS")) {
      if (response.keySet().contains("data")) {
        String referenceList = response.get("data").toString();

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Reference.class, new ReferenceDeserializer());
        mapper.registerModule(module);
        try {

          Reference[] references = mapper.readValue(referenceList, Reference[].class);
          return List.of(references);
        } catch (JsonMappingException e) {
          e.printStackTrace();
        } catch (JsonProcessingException e) {
          e.printStackTrace();
        }
      }
    }
    return new LinkedList<>();
  }
}
