# Literature Database

IPPOLIS WP3 literature database services.


## Deployment

When running this in the context of the other IPPOLIS WP3 tools (i.e. not standalone), you have to make sure to

- activate the Spring profile `eureka`, e.g. `export spring_profiles_active=eureka` if you are not using docker-compose
